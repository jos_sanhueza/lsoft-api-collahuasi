<?php
App::uses('AppModel', 'Model');
/**
 * Evaluacion Model
 *
 * @property Periodo $Periodo
 * @property CentralesCumplimiento $CentralesCumplimiento
 */
class Evaluacion extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';
	public $actsAs = array(
													"Containable",
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Periodo' => array(
			'className' => 'Periodo',
			'foreignKey' => 'periodo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CentralesCumplimiento' => array(
			'className' => 'CentralesCumplimiento',
			'foreignKey' => 'centrales_cumplimiento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
