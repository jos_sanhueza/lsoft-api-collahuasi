<?php
App::uses('AppModel', 'Model');
/**
 * Usuario Model
 *
 */
class Usuario extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	//public $useDbConfig = 'usuarios';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'username';

/**
 * Restricciones
 */
	public $validate = array(
		'username' => array(
			'rule'=>'isUnique',
			'message'=>'Este usuario ya existe.'
		)
	);


	/**
	 * ACL
	 */
	public $belongsTo = array("Rol", "Central");
	public $actsAs = array(
													"Acl"=>array("type"=>"requester"),
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);

	public function parentNode(){
		if(!$this->id && empty($this->data)) return null;
		$rolId = isset($this->data["Usuario"]["rol_id"]) ?
					$this->data["Usuario"]["rol_id"]: // if
					$this->field("rol_id"); // else

		if(!$rolId) return null;
		else return array("Rol"=>array("id"=> $rolId));
	}

	public function beforeSave($options = array()){
		if(isset($this->data[$this->alias]['password'])){
			//throw new InternalErrorException('Entra al if.');
			$this->data[$this->alias]['password'] = DigestAuthenticate::password(
				$this->data[$this->alias]['username'],
				$this->data[$this->alias]['password'],
				"lsoftColbun"
			);
		}
		return true;
	}



}
