<?php
App::uses('AppModel', 'Model');
/**
 * Criticidad Model
 *
 * @property CentralesCumplimiento $CentralesCumplimiento
 * @property Cumplimiento $Cumplimiento
 */
class Criticidad extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CentralesCumplimiento' => array(
			'className' => 'CentralesCumplimiento',
			'foreignKey' => 'criticidad_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Cumplimiento' => array(
			'className' => 'Cumplimiento',
			'foreignKey' => 'criticidad_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
