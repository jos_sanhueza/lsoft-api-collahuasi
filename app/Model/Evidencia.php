<?php
App::uses('AppModel', 'Model');
/**
 * Evidencia Model
 *
 * @property CentralesCuerpoLegal $CentralesCuerpoLegal
 * @property CentralesCumplimiento $CentralesCumplimiento
 */
class Evidencia extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';


	public $actsAs = array(
													"Containable",
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'CentralesCuerpoLegal' => array(
			'className' => 'CentralesCuerpoLegal',
			'joinTable' => 'centrales_cuerpo_legales_evidencias',
			'foreignKey' => 'evidencia_id',
			'associationForeignKey' => 'centrales_cuerpo_legal_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'CentralesCumplimiento' => array(
			'className' => 'CentralesCumplimiento',
			'joinTable' => 'centrales_cumplimientos_evidencias',
			'foreignKey' => 'evidencia_id',
			'associationForeignKey' => 'centrales_cumplimiento_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
