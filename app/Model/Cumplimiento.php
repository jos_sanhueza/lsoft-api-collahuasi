<?php
App::uses('AppModel', 'Model');
/**
 * Cumplimiento Model
 *
 * @property Articulo $Articulo
 * @property Frecuencia $Frecuencia
 * @property Criticidad $Criticidad
 * @property TipoRiesgo $TipoRiesgo
 * @property Central $Central
 */
class Cumplimiento extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'forma';
	public $actsAs = array(
													"Containable",
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Articulo' => array(
			'className' => 'Articulo',
			'foreignKey' => 'articulo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Frecuencia' => array(
			'className' => 'Frecuencia',
			'foreignKey' => 'frecuencia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Criticidad' => array(
			'className' => 'Criticidad',
			'foreignKey' => 'criticidad_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TipoRiesgo' => array(
			'className' => 'TipoRiesgo',
			'foreignKey' => 'tipo_riesgo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany
 */
	public $hasMany = array(
		'CentralesCumplimiento' => array(
			'className' => 'CentralesCumplimiento',
			'foreignKey' => 'cumplimiento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Central' => array(
			'className' => 'Central',
			'joinTable' => 'centrales_cumplimientos',
			'foreignKey' => 'cumplimiento_id',
			'associationForeignKey' => 'central_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
