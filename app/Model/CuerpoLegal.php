<?php
App::uses('AppModel', 'Model');
/**
 * CuerpoLegal Model
 *
 * @property TipoCuerpoLegal $TipoCuerpoLegal
 * @property Articulo $Articulo
 * @property Central $Central
 */
class CuerpoLegal extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';
public $actsAs = array(
												"Tools.Logable"=>array(
													"userModel"=>"Usuario"
												)
											);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'TipoCuerpoLegal' => array(
			'className' => 'TipoCuerpoLegal',
			'foreignKey' => 'tipo_cuerpo_legal_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tematica' => array(
			'className' => 'Tematica',
			'foreignKey' => 'tematica_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Articulo' => array(
			'className' => 'Articulo',
			'foreignKey' => 'cuerpo_legal_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Central' => array(
			'className' => 'Central',
			'joinTable' => 'centrales_cuerpo_legales',
			'foreignKey' => 'cuerpo_legal_id',
			'associationForeignKey' => 'central_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
