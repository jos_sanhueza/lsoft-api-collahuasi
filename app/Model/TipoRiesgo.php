<?php
App::uses('AppModel', 'Model');
/**
 * TipoRiesgo Model
 *
 * @property Cumplimiento $Cumplimiento
 */
class TipoRiesgo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';
	public $actsAs = array(
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Cumplimiento' => array(
			'className' => 'Cumplimiento',
			'foreignKey' => 'tipo_riesgo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
