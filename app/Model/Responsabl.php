<?php
App::uses('AppModel', 'Model');
/**
 * Responsabl Model
 *
 */
class Responsabl extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';
	public $actsAs = array(
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);

}
