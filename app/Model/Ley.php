<?php
App::uses("AppModel", "Model");
class Ley extends AppModel {
  public $useTable = "cuerpo_legales";
  public $actsAs = array(
                          "Tools.Logable"=>array(
                            "userModel"=>"Usuario"
                          )
                        );
  public $primaryKey ="idNorma";
  public $leyChileConfig = array(
    "urlCantidad"=>"http://www.leychile.cl/Consulta/obtxml?opt=60&",
    "urlResultados"=>"http://www.leychile.cl/Consulta/obtxml?opt=61&",
    "urlObtxml"=>"http://www.leychile.cl/Consulta/obtxml?opt=7&",
    "urlMetadatos"=>"http://www.leychile.cl/Consulta/obtxml?opt=4546&"
  );

/**
 * hasMany associations
 *
 * @var array
 */
  public $hasMany = array(
    'Articulo' => array(
      'className' => 'Articulo',
      'foreignKey' => 'cuerpo_legal_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    )
  );

/**
 * belongsTo associations
 *
 * @var array
 */
  public $belongsTo = array(
    'TipoCuerpoLegal' => array(
      'className' => 'TipoCuerpoLegal',
      'foreignKey' => 'tipo_cuerpo_legal_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    )
  );
/**
* Método cantidad.
*
* Entrega total de resultados para una búsqueda
* en webservice LeyChile.cl/Consulta/obtxml.
**/
  public function cantidad($cadena){
    $uri = $this->leyChileConfig["urlCantidad"]."cadena=$cadena";
    $xml = $this->curl($uri);
    $oXML = new SimpleXMLElement($xml);
    return $oXML->Total;
  }

/**
* Método resultados.
*
* Imprimer resultados para una búsqueda
* en webservice LeyChile.cl/Consulta/obtxml.
*
**/
  public function resultados($cadena, $cantidad){
    $uri = $this->leyChileConfig["urlResultados"]."cadena=$cadena&cantidad=$cantidad";
    $xml = $this->curl($uri);
    $oXML = new SimpleXMLElement($xml);
    return $oXML;
  }

  /**
  * Método recursivo que extrae los artículos
  * tanto transitorios como no transitorios de
  * un Cuerpo Legal en formato XML.
  * @param xml
  * @param articulos
  * @return array
  **/
  public function articulos($xml, $articulos = array()) {
    foreach($xml->EstructurasFuncionales->EstructuraFuncional as $k=>$v){
      if($v["tipoParte"] == "Artículo") {
        //print_r($v->Texto);die();
        $attributes = $v->attributes();
        $atributos = new stdClass();
        $articulo = new stdClass();
        foreach($attributes as $_k=>$_v) {
          $atributos->$_k = (string)$_v;
        }
        $articulo->Atributos = $atributos;
        $articulo->Metadatos = (array)$v->Metadatos;
        $articulo->Texto = (string)$v->Texto;
        if($v["transitorio"] == "transitorio") $articulos["transitorios"][] = $articulo;
        else $articulos["noTransitorios"][] = $articulo;
        if(isset($v->EstructurasFuncionales)) {
          $articulos = $this->articulos($v, $articulos);
        }
      }
      else {
        $articulos = $this->articulos($v, $articulos);
      }
    }
    return $articulos;
  }

/**
* Método metadatos.
*
* Obtiene metadatos de una ley.
**/

  public function metadatos($idNorma) {
    $uri = $this->leyChileConfig["urlMetadatos"]."idNorma=$idNorma";
    $xml = $this->curl($uri);
    $oXML = new SimpleXMLElement($xml);
    return $oXML;
  }

/**
* Método obtxml
*
* Obtiene XML de una norma,
* desde webservice LeyChile.cl/Consulta/obtxml
*
* @param idNorma
* @return oXML
**/
  public function obtxml($idNorma){
    $uri = $this->leyChileConfig["urlObtxml"]."idNorma=$idNorma";
    $xml = $this->curl($uri);
    $oXML = new SimpleXMLElement($xml);
    return $oXML;
  }

/**
* Método curl
*
* Realiza petición a una uri, mediante CURL.
*
* @param uri
* @param timeout
* @return xml
**/
  function curl($uri, $timeout = 0) {
    //set_time_limit(0);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $uri);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 0);
    $xml = curl_exec($ch);
    curl_close($ch);
    return $xml;
  }
}
