<?php
App::uses('AppModel', 'Model');
/**
 * CentralesCumplimiento Model
 *
 * @property Responsable $Responsable
 * @property Cumplimiento $Cumplimiento
 * @property Criticidad $Criticidad
 * @property Central $Central
 * @property Evaluacion $Evaluacion
 * @property Evidencia $Evidencia
 */
class CentralesCumplimiento extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';
	public $actsAs = array(
													"Containable",
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Responsabl' => array(
			'className' => 'Responsabl',
			'foreignKey' => 'responsable_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cumplimiento' => array(
			'className' => 'Cumplimiento',
			'foreignKey' => 'cumplimiento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Criticidad' => array(
			'className' => 'Criticidad',
			'foreignKey' => 'criticidad_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Central' => array(
			'className' => 'Central',
			'foreignKey' => 'central_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Evaluacion' => array(
			'className' => 'Evaluacion',
			'foreignKey' => 'centrales_cumplimiento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Mensaj' => array(
			'className' => 'Mensaj',
			'foreignKey' => 'centrales_cumplimiento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Evidencia' => array(
			'className' => 'Evidencia',
			'joinTable' => 'centrales_cumplimientos_evidencias',
			'foreignKey' => 'centrales_cumplimiento_id',
			'associationForeignKey' => 'evidencia_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
