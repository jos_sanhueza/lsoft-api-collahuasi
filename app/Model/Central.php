<?php
App::uses('AppModel', 'Model');
/**
 * Central Model
 *
 * @property Usuario $Usuario
 * @property CuerpoLegal $CuerpoLegal
 * @property Cumplimiento $Cumplimiento
 */
class Central extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';

/**
 * actsAs
 */
	public $actsAs = array(
		"Tools.Logable"=>array(
			"userModel"=>"Usuario"
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'central_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'CuerpoLegal' => array(
			'className' => 'CuerpoLegal',
			'joinTable' => 'centrales_cuerpo_legales',
			'foreignKey' => 'central_id',
			'associationForeignKey' => 'cuerpo_legal_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Cumplimiento' => array(
			'className' => 'Cumplimiento',
			'joinTable' => 'centrales_cumplimientos',
			'foreignKey' => 'central_id',
			'associationForeignKey' => 'cumplimiento_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
