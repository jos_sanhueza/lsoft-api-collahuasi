<?php
App::uses('AppModel', 'Model');
/**
 * CentralesCuerpoLegal Model
 *
 * @property CuerpoLegal $CuerpoLegal
 * @property Central $Central
 * @property Evidencia $Evidencia
 * @property Tematica $Tematica
 */
class CentralesCuerpoLegal extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $actsAs = array(
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CuerpoLegal' => array(
			'className' => 'CuerpoLegal',
			'foreignKey' => 'cuerpo_legal_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Central' => array(
			'className' => 'Central',
			'foreignKey' => 'central_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Evidencia' => array(
			'className' => 'Evidencia',
			'joinTable' => 'centrales_cuerpo_legales_evidencias',
			'foreignKey' => 'centrales_cuerpo_legal_id',
			'associationForeignKey' => 'evidencia_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Tematica' => array(
			'className' => 'Tematica',
			'joinTable' => 'centrales_cuerpo_legales_tematicas',
			'foreignKey' => 'centrales_cuerpo_legal_id',
			'associationForeignKey' => 'tematica_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
