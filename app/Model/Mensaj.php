<?php
App::uses('AppModel', 'Model');
/**
 * Mensaj Model
 *
 * @property Usuario $Usuario
 */
class Mensaj extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'pregunta';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Usuario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'usuario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Usuario2' => array(
			'className' => 'Usuario',
			'foreignKey' => 'usuario_id2',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CentralesCumplimiento' => array(
			'className' => 'CentralesCumplimiento',
			'foreignKey' => 'centrales_cumplimiento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $actsAs = array(
		"Containable",
		"Tools.Logable"=>array(
			"userModel"=>"Usuario"
		)
	);




}
