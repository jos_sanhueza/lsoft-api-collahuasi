<?php
App::uses('AppModel', 'Model');
/**
 * Frecuencia Model
 *
 * @property Cumplimiento $Cumplimiento
 */
class Frecuencia extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'valor';
	public $actsAs = array(
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Cumplimiento' => array(
			'className' => 'Cumplimiento',
			'foreignKey' => 'frecuencia_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
