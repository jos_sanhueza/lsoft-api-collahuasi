<?php
App::uses('AppModel', 'Model');
/**
 * Tematica Model
 *
 * @property CentralesCuerpoLegal $CentralesCuerpoLegal
 */
class Tematica extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';
	public $actsAs = array(
													"Containable",
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $belongsToMany = array(
		'CuerpoLegal' => array(
			'className' => 'CuerpoLegal',
			'foreignKey' => 'tematica_id',
			'unique' => 'keepExisting'
		)
	);

}
