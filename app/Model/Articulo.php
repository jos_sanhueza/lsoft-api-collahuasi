<?php
App::uses('AppModel', 'Model');
/**
 * Articulo Model
 *
 * @property CuerpoLegal $CuerpoLegal
 * @property Cumplimiento $Cumplimiento
 */
class Articulo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'numero';
	public $actsAs = array(
													"Tools.Logable"=>array(
														"userModel"=>"Usuario"
													)
												);
/**
 * Reglas de validación
 */
	public $validate = array(
		'idParte'=>array(
			'rule'=>'isUnique',
			'message'=>'Este artículo ya existe.'
		)
	);


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CuerpoLegal' => array(
			'className' => 'CuerpoLegal',
			'foreignKey' => 'cuerpo_legal_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Cumplimiento' => array(
			'className' => 'Cumplimiento',
			'foreignKey' => 'articulo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
