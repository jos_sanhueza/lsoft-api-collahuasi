<div class="evaluaciones index">
	<h2><?php echo __('Evaluaciones'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('centrales_cumplimiento_id'); ?></th>
			<th><?php echo $this->Paginator->sort('criticidad_id'); ?></th>
			<th><?php echo $this->Paginator->sort('periodo_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cumple_eval'); ?></th>
			<th><?php echo $this->Paginator->sort('evidencia_eval'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($evaluaciones as $evaluacion): ?>
	<tr>
		<td><?php echo h($evaluacion['Evaluacion']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($evaluacion['CentralesCumplimiento']['id'], array('controller' => 'centrales_cumplimientos', 'action' => 'view', $evaluacion['CentralesCumplimiento']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($evaluacion['Criticidad']['id'], array('controller' => 'criticidades', 'action' => 'view', $evaluacion['Criticidad']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($evaluacion['Periodo']['id'], array('controller' => 'periodos', 'action' => 'view', $evaluacion['Periodo']['id'])); ?>
		</td>
		<td><?php echo h($evaluacion['Evaluacion']['cumple_eval']); ?>&nbsp;</td>
		<td><?php echo h($evaluacion['Evaluacion']['evidencia_eval']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $evaluacion['Evaluacion']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $evaluacion['Evaluacion']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $evaluacion['Evaluacion']['id']), array(), __('Are you sure you want to delete # %s?', $evaluacion['Evaluacion']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Evaluacion'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Centrales Cumplimientos'), array('controller' => 'centrales_cumplimientos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Centrales Cumplimiento'), array('controller' => 'centrales_cumplimientos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Criticidades'), array('controller' => 'criticidades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Criticidad'), array('controller' => 'criticidades', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Periodos'), array('controller' => 'periodos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Periodo'), array('controller' => 'periodos', 'action' => 'add')); ?> </li>
	</ul>
</div>
