<div class="evaluaciones form">
<?php echo $this->Form->create('Evaluacion'); ?>
	<fieldset>
		<legend><?php echo __('Edit Evaluacion'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('centrales_cumplimiento_id');
		echo $this->Form->input('criticidad_id');
		echo $this->Form->input('periodo_id');
		echo $this->Form->input('cumple_eval');
		echo $this->Form->input('evidencia_eval');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Evaluacion.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Evaluacion.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Evaluaciones'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Centrales Cumplimientos'), array('controller' => 'centrales_cumplimientos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Centrales Cumplimiento'), array('controller' => 'centrales_cumplimientos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Criticidades'), array('controller' => 'criticidades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Criticidad'), array('controller' => 'criticidades', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Periodos'), array('controller' => 'periodos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Periodo'), array('controller' => 'periodos', 'action' => 'add')); ?> </li>
	</ul>
</div>
