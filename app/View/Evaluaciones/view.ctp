<div class="evaluaciones view">
<h2><?php echo __('Evaluacion'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($evaluacion['Evaluacion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Centrales Cumplimiento'); ?></dt>
		<dd>
			<?php echo $this->Html->link($evaluacion['CentralesCumplimiento']['id'], array('controller' => 'centrales_cumplimientos', 'action' => 'view', $evaluacion['CentralesCumplimiento']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Criticidad'); ?></dt>
		<dd>
			<?php echo $this->Html->link($evaluacion['Criticidad']['id'], array('controller' => 'criticidades', 'action' => 'view', $evaluacion['Criticidad']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Periodo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($evaluacion['Periodo']['id'], array('controller' => 'periodos', 'action' => 'view', $evaluacion['Periodo']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cumple Eval'); ?></dt>
		<dd>
			<?php echo h($evaluacion['Evaluacion']['cumple_eval']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Evidencia Eval'); ?></dt>
		<dd>
			<?php echo h($evaluacion['Evaluacion']['evidencia_eval']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Evaluacion'), array('action' => 'edit', $evaluacion['Evaluacion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Evaluacion'), array('action' => 'delete', $evaluacion['Evaluacion']['id']), array(), __('Are you sure you want to delete # %s?', $evaluacion['Evaluacion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Evaluaciones'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Evaluacion'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales Cumplimientos'), array('controller' => 'centrales_cumplimientos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Centrales Cumplimiento'), array('controller' => 'centrales_cumplimientos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Criticidades'), array('controller' => 'criticidades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Criticidad'), array('controller' => 'criticidades', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Periodos'), array('controller' => 'periodos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Periodo'), array('controller' => 'periodos', 'action' => 'add')); ?> </li>
	</ul>
</div>
