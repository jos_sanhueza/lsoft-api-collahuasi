<div class="evidencias index">
	<h2><?php echo __('Evidencias'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('ubicacion'); ?></th>
			<th><?php echo $this->Paginator->sort('mimetype'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($evidencias as $evidencia): ?>
	<tr>
		<td><?php echo h($evidencia['Evidencia']['id']); ?>&nbsp;</td>
		<td><?php echo h($evidencia['Evidencia']['nombre']); ?>&nbsp;</td>
		<td><?php echo h($evidencia['Evidencia']['ubicacion']); ?>&nbsp;</td>
		<td><?php echo h($evidencia['Evidencia']['mimetype']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $evidencia['Evidencia']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $evidencia['Evidencia']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $evidencia['Evidencia']['id']), array(), __('Are you sure you want to delete # %s?', $evidencia['Evidencia']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Evidencia'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Cumplimientos'), array('controller' => 'cumplimientos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cumplimiento'), array('controller' => 'cumplimientos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales Cuerpo Legales'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Centrales Cuerpo Legal'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'add')); ?> </li>
	</ul>
</div>
