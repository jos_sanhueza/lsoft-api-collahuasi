<div class="evidencias form">
<?php echo $this->Form->create('Evidencia'); ?>
	<fieldset>
		<legend><?php echo __('Add Evidencia'); ?></legend>
	<?php
		echo $this->Form->input('nombre');
		echo $this->Form->input('ubicacion');
		echo $this->Form->input('mimetype');
		echo $this->Form->input('Cumplimiento');
		echo $this->Form->input('CentralesCuerpoLegal');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Evidencias'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Cumplimientos'), array('controller' => 'cumplimientos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cumplimiento'), array('controller' => 'cumplimientos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales Cuerpo Legales'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Centrales Cuerpo Legal'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'add')); ?> </li>
	</ul>
</div>
