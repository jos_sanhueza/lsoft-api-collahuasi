<div class="evidencias view">
<h2><?php echo __('Evidencia'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($evidencia['Evidencia']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($evidencia['Evidencia']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ubicacion'); ?></dt>
		<dd>
			<?php echo h($evidencia['Evidencia']['ubicacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mimetype'); ?></dt>
		<dd>
			<?php echo h($evidencia['Evidencia']['mimetype']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Evidencia'), array('action' => 'edit', $evidencia['Evidencia']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Evidencia'), array('action' => 'delete', $evidencia['Evidencia']['id']), array(), __('Are you sure you want to delete # %s?', $evidencia['Evidencia']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Evidencias'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Evidencia'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cumplimientos'), array('controller' => 'cumplimientos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cumplimiento'), array('controller' => 'cumplimientos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales Cuerpo Legales'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Centrales Cuerpo Legal'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Cumplimientos'); ?></h3>
	<?php if (!empty($evidencia['Cumplimiento'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Articulo Id'); ?></th>
		<th><?php echo __('Frecuencia Id'); ?></th>
		<th><?php echo __('Descripcion'); ?></th>
		<th><?php echo __('Forma'); ?></th>
		<th><?php echo __('Permiso Asociado'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($evidencia['Cumplimiento'] as $cumplimiento): ?>
		<tr>
			<td><?php echo $cumplimiento['id']; ?></td>
			<td><?php echo $cumplimiento['articulo_id']; ?></td>
			<td><?php echo $cumplimiento['frecuencia_id']; ?></td>
			<td><?php echo $cumplimiento['descripcion']; ?></td>
			<td><?php echo $cumplimiento['forma']; ?></td>
			<td><?php echo $cumplimiento['permiso_asociado']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cumplimientos', 'action' => 'view', $cumplimiento['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cumplimientos', 'action' => 'edit', $cumplimiento['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cumplimientos', 'action' => 'delete', $cumplimiento['id']), array(), __('Are you sure you want to delete # %s?', $cumplimiento['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Cumplimiento'), array('controller' => 'cumplimientos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Centrales Cuerpo Legales'); ?></h3>
	<?php if (!empty($evidencia['CentralesCuerpoLegal'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Cuerpo Legal Id'); ?></th>
		<th><?php echo __('Central Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Startdate'); ?></th>
		<th><?php echo __('Duedate'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Excel'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($evidencia['CentralesCuerpoLegal'] as $centralesCuerpoLegal): ?>
		<tr>
			<td><?php echo $centralesCuerpoLegal['id']; ?></td>
			<td><?php echo $centralesCuerpoLegal['cuerpo_legal_id']; ?></td>
			<td><?php echo $centralesCuerpoLegal['central_id']; ?></td>
			<td><?php echo $centralesCuerpoLegal['name']; ?></td>
			<td><?php echo $centralesCuerpoLegal['startdate']; ?></td>
			<td><?php echo $centralesCuerpoLegal['duedate']; ?></td>
			<td><?php echo $centralesCuerpoLegal['status']; ?></td>
			<td><?php echo $centralesCuerpoLegal['excel']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'view', $centralesCuerpoLegal['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'edit', $centralesCuerpoLegal['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'delete', $centralesCuerpoLegal['id']), array(), __('Are you sure you want to delete # %s?', $centralesCuerpoLegal['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Centrales Cuerpo Legal'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
