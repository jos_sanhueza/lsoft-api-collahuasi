<div class="articulos view">
<h2><?php echo __('Articulo'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($articulo['Articulo']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nivel Riesgo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($articulo['NivelRiesgo']['id'], array('controller' => 'nivel_riesgos', 'action' => 'view', $articulo['NivelRiesgo']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Frecuencia'); ?></dt>
		<dd>
			<?php echo $this->Html->link($articulo['Frecuencia']['id'], array('controller' => 'frecuencias', 'action' => 'view', $articulo['Frecuencia']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cuerpo Legal'); ?></dt>
		<dd>
			<?php echo $this->Html->link($articulo['CuerpoLegal']['nombre'], array('controller' => 'cuerpo_legales', 'action' => 'view', $articulo['CuerpoLegal']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Numero'); ?></dt>
		<dd>
			<?php echo h($articulo['Articulo']['numero']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Descripcion'); ?></dt>
		<dd>
			<?php echo h($articulo['Articulo']['descripcion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observacion'); ?></dt>
		<dd>
			<?php echo h($articulo['Articulo']['observacion']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Articulo'), array('action' => 'edit', $articulo['Articulo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Articulo'), array('action' => 'delete', $articulo['Articulo']['id']), array(), __('Are you sure you want to delete # %s?', $articulo['Articulo']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Articulos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Articulo'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Nivel Riesgos'), array('controller' => 'nivel_riesgos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Nivel Riesgo'), array('controller' => 'nivel_riesgos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frecuencias'), array('controller' => 'frecuencias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frecuencia'), array('controller' => 'frecuencias', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cuerpo Legales'), array('controller' => 'cuerpo_legales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cuerpo Legal'), array('controller' => 'cuerpo_legales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cumplimientos'), array('controller' => 'cumplimientos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cumplimiento'), array('controller' => 'cumplimientos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Cumplimientos'); ?></h3>
	<?php if (!empty($articulo['Cumplimiento'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Articulo Id'); ?></th>
		<th><?php echo __('Frecuencia Id'); ?></th>
		<th><?php echo __('Descripcion'); ?></th>
		<th><?php echo __('Forma'); ?></th>
		<th><?php echo __('Permiso Asociado'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($articulo['Cumplimiento'] as $cumplimiento): ?>
		<tr>
			<td><?php echo $cumplimiento['id']; ?></td>
			<td><?php echo $cumplimiento['articulo_id']; ?></td>
			<td><?php echo $cumplimiento['frecuencia_id']; ?></td>
			<td><?php echo $cumplimiento['descripcion']; ?></td>
			<td><?php echo $cumplimiento['forma']; ?></td>
			<td><?php echo $cumplimiento['permiso_asociado']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cumplimientos', 'action' => 'view', $cumplimiento['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cumplimientos', 'action' => 'edit', $cumplimiento['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cumplimientos', 'action' => 'delete', $cumplimiento['id']), array(), __('Are you sure you want to delete # %s?', $cumplimiento['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Cumplimiento'), array('controller' => 'cumplimientos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
