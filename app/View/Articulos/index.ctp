<div class="articulos index">
	<h2><?php echo __('Articulos'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nivel_riesgo_id'); ?></th>
			<th><?php echo $this->Paginator->sort('frecuencia_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cuerpo_legal_id'); ?></th>
			<th><?php echo $this->Paginator->sort('numero'); ?></th>
			<th><?php echo $this->Paginator->sort('descripcion'); ?></th>
			<th><?php echo $this->Paginator->sort('observacion'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($articulos as $articulo): ?>
	<tr>
		<td><?php echo h($articulo['Articulo']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($articulo['NivelRiesgo']['id'], array('controller' => 'nivel_riesgos', 'action' => 'view', $articulo['NivelRiesgo']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($articulo['Frecuencia']['id'], array('controller' => 'frecuencias', 'action' => 'view', $articulo['Frecuencia']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($articulo['CuerpoLegal']['nombre'], array('controller' => 'cuerpo_legales', 'action' => 'view', $articulo['CuerpoLegal']['id'])); ?>
		</td>
		<td><?php echo h($articulo['Articulo']['numero']); ?>&nbsp;</td>
		<td><?php echo h($articulo['Articulo']['descripcion']); ?>&nbsp;</td>
		<td><?php echo h($articulo['Articulo']['observacion']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $articulo['Articulo']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $articulo['Articulo']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $articulo['Articulo']['id']), array(), __('Are you sure you want to delete # %s?', $articulo['Articulo']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Articulo'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Nivel Riesgos'), array('controller' => 'nivel_riesgos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Nivel Riesgo'), array('controller' => 'nivel_riesgos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frecuencias'), array('controller' => 'frecuencias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frecuencia'), array('controller' => 'frecuencias', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cuerpo Legales'), array('controller' => 'cuerpo_legales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cuerpo Legal'), array('controller' => 'cuerpo_legales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cumplimientos'), array('controller' => 'cumplimientos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cumplimiento'), array('controller' => 'cumplimientos', 'action' => 'add')); ?> </li>
	</ul>
</div>
