<div class="centrales view">
<h2><?php echo __('Central'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($central['Central']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($central['Central']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ubicacion'); ?></dt>
		<dd>
			<?php echo h($central['Central']['ubicacion']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Central'), array('action' => 'edit', $central['Central']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Central'), array('action' => 'delete', $central['Central']['id']), array(), __('Are you sure you want to delete # %s?', $central['Central']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Central'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Usuarios'), array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Usuario'), array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cuerpo Legales'), array('controller' => 'cuerpo_legales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cuerpo Legal'), array('controller' => 'cuerpo_legales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales Cuerpo Legales'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Centrales Cuerpo Legal'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Usuarios'); ?></h3>
	<?php if (!empty($central['Usuario'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Apellido'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Central Id'); ?></th>
		<th><?php echo __('Rol Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($central['Usuario'] as $usuario): ?>
		<tr>
			<td><?php echo $usuario['id']; ?></td>
			<td><?php echo $usuario['username']; ?></td>
			<td><?php echo $usuario['password']; ?></td>
			<td><?php echo $usuario['nombre']; ?></td>
			<td><?php echo $usuario['apellido']; ?></td>
			<td><?php echo $usuario['email']; ?></td>
			<td><?php echo $usuario['central_id']; ?></td>
			<td><?php echo $usuario['rol_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'usuarios', 'action' => 'view', $usuario['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'usuarios', 'action' => 'edit', $usuario['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'usuarios', 'action' => 'delete', $usuario['id']), array(), __('Are you sure you want to delete # %s?', $usuario['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Usuario'), array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Cuerpo Legales'); ?></h3>
	<?php if (!empty($central['CuerpoLegal'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Tipo Cuerpo Legal Id'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Descripcion'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($central['CuerpoLegal'] as $cuerpoLegal): ?>
		<tr>
			<td><?php echo $cuerpoLegal['id']; ?></td>
			<td><?php echo $cuerpoLegal['tipo_cuerpo_legal_id']; ?></td>
			<td><?php echo $cuerpoLegal['nombre']; ?></td>
			<td><?php echo $cuerpoLegal['descripcion']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cuerpo_legales', 'action' => 'view', $cuerpoLegal['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cuerpo_legales', 'action' => 'edit', $cuerpoLegal['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cuerpo_legales', 'action' => 'delete', $cuerpoLegal['id']), array(), __('Are you sure you want to delete # %s?', $cuerpoLegal['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Cuerpo Legal'), array('controller' => 'cuerpo_legales', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Centrales Cuerpo Legales'); ?></h3>
	<?php if (!empty($central['CentralesCuerpoLegal'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Cuerpo Legal Id'); ?></th>
		<th><?php echo __('Central Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Startdate'); ?></th>
		<th><?php echo __('Duedate'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Excel'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($central['CentralesCuerpoLegal'] as $centralesCuerpoLegal): ?>
		<tr>
			<td><?php echo $centralesCuerpoLegal['id']; ?></td>
			<td><?php echo $centralesCuerpoLegal['cuerpo_legal_id']; ?></td>
			<td><?php echo $centralesCuerpoLegal['central_id']; ?></td>
			<td><?php echo $centralesCuerpoLegal['name']; ?></td>
			<td><?php echo $centralesCuerpoLegal['startdate']; ?></td>
			<td><?php echo $centralesCuerpoLegal['duedate']; ?></td>
			<td><?php echo $centralesCuerpoLegal['status']; ?></td>
			<td><?php echo $centralesCuerpoLegal['excel']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'view', $centralesCuerpoLegal['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'edit', $centralesCuerpoLegal['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'delete', $centralesCuerpoLegal['id']), array(), __('Are you sure you want to delete # %s?', $centralesCuerpoLegal['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Centrales Cuerpo Legal'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
