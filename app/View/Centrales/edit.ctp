<div class="centrales form">
<?php echo $this->Form->create('Central'); ?>
	<fieldset>
		<legend><?php echo __('Edit Central'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nombre');
		echo $this->Form->input('ubicacion');
		echo $this->Form->input('CuerpoLegal');
		echo $this->Form->input('CentralesCuerpoLegal');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Central.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Central.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Centrales'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Usuarios'), array('controller' => 'usuarios', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Usuario'), array('controller' => 'usuarios', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cuerpo Legales'), array('controller' => 'cuerpo_legales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cuerpo Legal'), array('controller' => 'cuerpo_legales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales Cuerpo Legales'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Centrales Cuerpo Legal'), array('controller' => 'centrales_cuerpo_legales', 'action' => 'add')); ?> </li>
	</ul>
</div>
