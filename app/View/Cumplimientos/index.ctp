<div class="cumplimientos index">
	<h2><?php echo __('Cumplimientos'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('articulo_id'); ?></th>
			<th><?php echo $this->Paginator->sort('frecuencia_id'); ?></th>
			<th><?php echo $this->Paginator->sort('criticidad_id'); ?></th>
			<th><?php echo $this->Paginator->sort('tipo_riesgo_id'); ?></th>
			<th><?php echo $this->Paginator->sort('forma'); ?></th>
			<th><?php echo $this->Paginator->sort('permiso_asociado'); ?></th>
			<th><?php echo $this->Paginator->sort('descripcion'); ?></th>
			<th><?php echo $this->Paginator->sort('riesgo_valor'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($cumplimientos as $cumplimiento): ?>
	<tr>
		<td><?php echo h($cumplimiento['Cumplimiento']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($cumplimiento['Articulo']['numero'], array('controller' => 'articulos', 'action' => 'view', $cumplimiento['Articulo']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($cumplimiento['Frecuencia']['valor'], array('controller' => 'frecuencias', 'action' => 'view', $cumplimiento['Frecuencia']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($cumplimiento['Criticidad']['id'], array('controller' => 'criticidades', 'action' => 'view', $cumplimiento['Criticidad']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($cumplimiento['TipoRiesgo']['nombre'], array('controller' => 'tipo_riesgos', 'action' => 'view', $cumplimiento['TipoRiesgo']['id'])); ?>
		</td>
		<td><?php echo h($cumplimiento['Cumplimiento']['forma']); ?>&nbsp;</td>
		<td><?php echo h($cumplimiento['Cumplimiento']['permiso_asociado']); ?>&nbsp;</td>
		<td><?php echo h($cumplimiento['Cumplimiento']['descripcion']); ?>&nbsp;</td>
		<td><?php echo h($cumplimiento['Cumplimiento']['riesgo_valor']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $cumplimiento['Cumplimiento']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $cumplimiento['Cumplimiento']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $cumplimiento['Cumplimiento']['id']), array(), __('Are you sure you want to delete # %s?', $cumplimiento['Cumplimiento']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Cumplimiento'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Articulos'), array('controller' => 'articulos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Articulo'), array('controller' => 'articulos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frecuencias'), array('controller' => 'frecuencias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frecuencia'), array('controller' => 'frecuencias', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Criticidades'), array('controller' => 'criticidades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Criticidad'), array('controller' => 'criticidades', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipo Riesgos'), array('controller' => 'tipo_riesgos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipo Riesgo'), array('controller' => 'tipo_riesgos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales'), array('controller' => 'centrales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Central'), array('controller' => 'centrales', 'action' => 'add')); ?> </li>
	</ul>
</div>
