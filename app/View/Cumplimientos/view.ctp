<div class="cumplimientos view">
<h2><?php echo __('Cumplimiento'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($cumplimiento['Cumplimiento']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Articulo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cumplimiento['Articulo']['numero'], array('controller' => 'articulos', 'action' => 'view', $cumplimiento['Articulo']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Frecuencia'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cumplimiento['Frecuencia']['valor'], array('controller' => 'frecuencias', 'action' => 'view', $cumplimiento['Frecuencia']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Criticidad'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cumplimiento['Criticidad']['id'], array('controller' => 'criticidades', 'action' => 'view', $cumplimiento['Criticidad']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo Riesgo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cumplimiento['TipoRiesgo']['nombre'], array('controller' => 'tipo_riesgos', 'action' => 'view', $cumplimiento['TipoRiesgo']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Forma'); ?></dt>
		<dd>
			<?php echo h($cumplimiento['Cumplimiento']['forma']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Permiso Asociado'); ?></dt>
		<dd>
			<?php echo h($cumplimiento['Cumplimiento']['permiso_asociado']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Descripcion'); ?></dt>
		<dd>
			<?php echo h($cumplimiento['Cumplimiento']['descripcion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Riesgo Valor'); ?></dt>
		<dd>
			<?php echo h($cumplimiento['Cumplimiento']['riesgo_valor']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cumplimiento'), array('action' => 'edit', $cumplimiento['Cumplimiento']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cumplimiento'), array('action' => 'delete', $cumplimiento['Cumplimiento']['id']), array(), __('Are you sure you want to delete # %s?', $cumplimiento['Cumplimiento']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cumplimientos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cumplimiento'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Articulos'), array('controller' => 'articulos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Articulo'), array('controller' => 'articulos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frecuencias'), array('controller' => 'frecuencias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frecuencia'), array('controller' => 'frecuencias', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Criticidades'), array('controller' => 'criticidades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Criticidad'), array('controller' => 'criticidades', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipo Riesgos'), array('controller' => 'tipo_riesgos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipo Riesgo'), array('controller' => 'tipo_riesgos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales'), array('controller' => 'centrales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Central'), array('controller' => 'centrales', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Centrales'); ?></h3>
	<?php if (!empty($cumplimiento['Central'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Ubicacion'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cumplimiento['Central'] as $central): ?>
		<tr>
			<td><?php echo $central['id']; ?></td>
			<td><?php echo $central['nombre']; ?></td>
			<td><?php echo $central['ubicacion']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'centrales', 'action' => 'view', $central['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'centrales', 'action' => 'edit', $central['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'centrales', 'action' => 'delete', $central['id']), array(), __('Are you sure you want to delete # %s?', $central['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Central'), array('controller' => 'centrales', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
