<div class="cumplimientos form">
<?php echo $this->Form->create('Cumplimiento'); ?>
	<fieldset>
		<legend><?php echo __('Add Cumplimiento'); ?></legend>
	<?php
		echo $this->Form->input('articulo_id');
		echo $this->Form->input('frecuencia_id');
		echo $this->Form->input('criticidad_id');
		echo $this->Form->input('tipo_riesgo_id');
		echo $this->Form->input('forma');
		echo $this->Form->input('permiso_asociado');
		echo $this->Form->input('descripcion');
		echo $this->Form->input('riesgo_valor');
		echo $this->Form->input('Central');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Cumplimientos'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Articulos'), array('controller' => 'articulos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Articulo'), array('controller' => 'articulos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frecuencias'), array('controller' => 'frecuencias', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frecuencia'), array('controller' => 'frecuencias', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Criticidades'), array('controller' => 'criticidades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Criticidad'), array('controller' => 'criticidades', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipo Riesgos'), array('controller' => 'tipo_riesgos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipo Riesgo'), array('controller' => 'tipo_riesgos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Centrales'), array('controller' => 'centrales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Central'), array('controller' => 'centrales', 'action' => 'add')); ?> </li>
	</ul>
</div>
