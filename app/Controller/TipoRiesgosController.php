<?php
App::uses('AppController', 'Controller');
/**
 * TipoRiesgos Controller
 *
 * @property TipoRiesgo $TipoRiesgo
 * @property PaginatorComponent $Paginator
 * @property "RequestHandler"Component $"RequestHandler"
 * @property SessionComponent $Session
 */
class TipoRiesgosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TipoRiesgo->recursive = 0;
		$this->set('tipoRiesgos', $this->Paginator->paginate());
		$this->set('_serialize', 'tipoRiesgos');
	}

	/**
	* Entrega objeto con listado de tipos de riesgo.
	* Sirve para llenar los SelectPicker.
	* @return null
	**/
	public function selectPicker() {
		$this->TipoRiesgo->recursive = 0;
		$this->set('tiporiesgos', $this->TipoRiesgo->find('all',
				array(
					'fields'=>array(
						'id',
						'nombre'
					)
				)
			)
		);
		$this->set('_serialize', 'tiporiesgos');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TipoRiesgo->exists($id)) {
			throw new NotFoundException(__('Invalid tipo riesgo'));
		}
		$options = array('conditions' => array('TipoRiesgo.' . $this->TipoRiesgo->primaryKey => $id));
		$this->set('tipoRiesgo', $this->TipoRiesgo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TipoRiesgo->create();
			if ($tipoRiesgo = $this->TipoRiesgo->save($this->request->data)) {
				$this->set("tipoRiesgo", $tipoRiesgo);
				$this->set("_serialize", "tipoRiesgo");
			} else {
				throw new BadRequestException("No se ha podido guardar el tipo de riesgo.");
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->TipoRiesgo->exists($id)) {
			throw new NotFoundException(__('No existe ese tipo de riesgo.'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($tipo_riesgo = $this->TipoRiesgo->save($this->request->data)) {
				$this->set('tipo_riesgo', $tipo_riesgo);
				$this->set('_serialize', 'tipo_riesgo');

			} else {
				throw new BadRequestExcepcion("Error al guardar los datos del tipo de riesgo.");
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->TipoRiesgo->id = $id;
		if (!$this->TipoRiesgo->exists()) {
			throw new NotFoundException(__('Ese Tipo de Riesgo no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->TipoRiesgo->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}
}
