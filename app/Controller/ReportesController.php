<?php
App::uses('AppController', 'Controller');
/**
 * Usuarios Controller
 *
 * @property Usuario $Usuario
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReportesController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public $components = array('Paginator', 'Session', 'RequestHandler');

/**
 * Método cumplimiento_empresa
 *
 * Entrega información estadística de la empresa.
 */
	public function cumplimiento_empresa(){
		$fin = $this->data["fin"];
		$query = "CALL `cumplimiento_empresa`('$fin')";

		$data = $this->Reporte->query($query);
		$this->set("data", $data[0][0]);
		$this->set("_serialize", "data");
	}

/**
 * Método cumplimiento central
 *
 * Entrega información estadística sobre la empresa
 */
	public function cumplimiento_central(){
		$fin = $this->data["fin"];
		$query = "CALL `cumplimiento_central`('$fin')";
		$data = $this->Reporte->query($query);
		foreach($data as $r){
			$res[] = array("Central"=>$r["tt"], "datos"=>$r[0]);
		}
		if(sizeof($data) < 1) {
			$res = [];
		}
		$this->set("data", $res);
		$this->set("_serialize", "data");
	}

/**
 * Método cumplimiento_tematica
 *
 * Entrega información estadística sobre
 * las temáticas, dentro de una central.
 */
	public function cumplimiento_tematica(){
		$fin = $this->data["fin"];
		$central_id = $this->data["central_id"];
		$query = "CALL `cumplimiento_tematica`($central_id, '$fin')";

		$data = $this->Reporte->query($query);
		foreach($data as $r){
			$res[] = array("Tematica"=>$r["tt"], "datos"=>$r[0]);
		}
		if(sizeof($data) < 1) {
			$res = [];
		}
		$this->set("data", $res);
		$this->set("_serialize", "data");
	}

/**
 * Método cumplimiento_cuerpo_legal
 *
 * Entrega información estadística
 */
	public function cumplimiento_cuerpo_legal(){
		$fin = $this->request->query["fin"];
		$central_id = $this->request->query["central_id"];
		$tematica_id = $this->request->query["tematica_id"];

		$query = "CALL `cumplimiento_cuerpo_legal`($central_id, $tematica_id, '$fin')";
		$data = $this->Reporte->query($query);
		foreach($data as $r){
			$res[] = array("ley"=>$r["tt"], "datos"=>$r[0]);
		}
		if(sizeof($data) < 1) {
			$res = [];
		}
		$this->set("data", $res);
		$this->set("_serialize", "data");
	}


/**
 * Método criticidad_empresa
 */
	public function criticidad_empresa(){
		$fin = $this->data["fin"];
		$query = "CALL `criticidad_empresa`('$fin')";

		$data = $this->Reporte->query($query);
		$this->set("data", $data[0]);
		$this->set("_serialize", "data");
	}
/**
 * Método criticidad_central
 */
	public function criticidad_central(){
		$fin = $this->data["fin"];
		$query = "CALL `criticidad_central`('$fin')";

		$data = $this->Reporte->query($query);
		foreach($data as $r){
			$res[] = array("Central"=>$r["t"], "datos"=>$r[0]);
		}
		if(sizeof($data) < 1) {
			$res = [];
		}
		$this->set("data", $res);
		$this->set("_serialize", "data");
	}
/**
 * Método criticidad_tematica
 */
	public function criticidad_tematica(){
		$central_id = $this->data["central_id"];
		$fin = $this->data["fin"];
		$query = "CALL `criticidad_tematica`($central_id, '$fin')";

		$data = $this->Reporte->query($query);
		foreach($data as $r){
			$res[] = array("Tematica"=>$r["t"], "datos"=>$r[0]);
		}
		if(sizeof($data) < 1) {
			$res = [];
		}
		$this->set("data", $res);
		$this->set("_serialize", "data");
	}
/**
 * Método criticidad_cuerpo_legal
 */
	public function criticidad_cuerpo_legal(){
		$central_id = $this->request->query["central_id"];
		$tematica_id = $this->request->query["tematica_id"];
		$fin = $this->request->query["fin"];
		$query = "CALL `criticidad_cuerpo_legal`($central_id, $tematica_id, '$fin')";

		$data = $this->Reporte->query($query);
		foreach($data as $r){
			$res[] = array("ley"=>$r["t"], "datos"=>$r[0]);
		}
		if(sizeof($data) < 1) {
			$res = [];
		}
		$this->set("data", $res);
		$this->set("_serialize", "data");
	}

/**
 * Método cumplimiento_tematica_empresa
 */
	function cumplimiento_tematica_empresa(){
		$fin = $this->request->query["fin"];
		$query = "CALL `cumplimiento_tematica_empresa` ('$fin');";

		$data = $this->Reporte->query($query);
		foreach($data as $r) {
			$res[] = array("datos"=>$r[0], "Tematica"=>$r["t"]);
		}
		if(sizeof($data) < 1) {
			$res = [];
		}
		$this->set("data", $res);
		$this->set("_serialize", "data");
	}

/**
 * Método criticidad_tematic_empresa
 */
	function criticidad_tematica_empresa(){
		$fin = $this->request->query["fin"];
		$query = "CALL `criticidad_tematica_empresa` ('$fin');";
		// debug($query);
		$data = $this->Reporte->query($query);
		foreach($data as $r) {
			$res[] = array("datos"=>$r[0], "Tematica"=>$r["t"]);
		}
		if(sizeof($data) < 1) {
			$res = [];
		}
		$this->set("data", $res);
		$this->set("_serialize", "data");
	}

/**
 * Método xls_ccl
 *
 * Genera reporte en excel, sobre los cumplimientos
 * de un cuerpo legal, dentro de una central, para una determinada fecha.
 */
	public function xls_ccl($central_id = false, $cuerpo_legal_id = false, $fin = false){
		//www.lsoft.cl/lsoft_api/reportes/xls_ccl/?central_id=1&cuerpo_legal_id=12&fin=2015-2-31
		if(!$central_id){
			$central_id = $this->request->query("central_id");
		}
		if(!$cuerpo_legal_id) {
			$cuerpo_legal_id = $this->request->query("cuerpo_legal_id");
		}

		if(isset($this->request->query["fin"]) && strlen($this->request->query["fin"]) > 0)
		{
			$fin = $this->request->query("fin");
			//var_dump($fin, "adfasd");
		}
		else
		{
			$fin = date("Y-m-d");
			//var_dump($fin);
		}

		App::import('Vendor', 'Classes/PHPExcel');

		$q_evaluaciones = "select *
							from evaluaciones Evaluacion
							inner join (
								SELECT  max(id) as id
								FROM    evaluaciones
								WHERE   fecha <= '$fin'
								GROUP   BY centrales_cumplimiento_id
							) ev
							on Evaluacion.id = ev.id
							join centrales_cumplimientos CentralCumplimiento
							on Evaluacion.centrales_cumplimiento_id = CentralCumplimiento.id
							join centrales Central
							on CentralCumplimiento.central_id = Central.id
							join cumplimientos Cumplimiento
							on Cumplimiento.id = CentralCumplimiento.cumplimiento_id
							join articulos Articulo
							on Articulo.id = Cumplimiento.articulo_id
							join cuerpo_legales CuerpoLegal
							on CuerpoLegal.id = Articulo.cuerpo_legal_id
							join tipo_cuerpo_legales TipoCuerpoLegal
							on CuerpoLegal.tipo_cuerpo_legal_id = TipoCuerpoLegal.id
							join responsables Responsable
							on Responsable.id = CentralCumplimiento.responsable_id
							join frecuencias Frecuencia
							on Cumplimiento.frecuencia_id = Frecuencia.id
							where CuerpoLegal.id = $cuerpo_legal_id
							and CentralCumplimiento.central_id = $central_id
							and Evaluacion.fecha <= '$fin'
							order by cast(Articulo.numero as unsigned) asc
							";
		$evaluaciones = $this->Reporte->query($q_evaluaciones);
		// var_dump($q_evaluaciones);
		$cntAplicables = 0;

		foreach($evaluaciones as $ev=>$al){
			if($al["Evaluacion"]["cumple_eval"] != "NA") $cntAplicables++; // cuenta articulos aplicables
		}

		$objPHPExcel = new PHPExcel();

		$objReader = PHPExcel_IOFactory::createReader('Excel5');
		$objPHPExcel = $objReader->load(WWW_ROOT . "xls/template.xls");


		$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('C10', count($evaluaciones))
								->setCellValue('C11', $cntAplicables);

		$i = 14;

		$styleArray = array(
		  'borders' => array(
		    'allborders' => array(
		      'style' => PHPExcel_Style_Border::BORDER_THIN
		    )
		  )
		);

		foreach($evaluaciones as $e){
			$i++;
			$formula = '=IF(I'.$i.'="NE","NE",IF(I'.$i.'="Si",1,0))';
			$central = $e["Central"];
			$cuerpoLegal = $e["CuerpoLegal"];
			$tipoCuerpoLegal = $e["TipoCuerpoLegal"];
			$_frecuencia_valor = ($e["Frecuencia"]["valor"] != "") ? $e["Frecuencia"]["valor"] : "--";
			$objPHPExcel->getActiveSheet()
									->setCellValue('A'.$i, $e["Articulo"]["numero"])
									->setCellValue('B'.$i, $e["Cumplimiento"]["descripcion"])
									->setCellValue('C'.$i, $e["Responsable"]["nombre"])
									->setCellValue('D'.$i, $e["Cumplimiento"]["permiso_asociado"])
									->setCellValue('E'.$i, $e["Cumplimiento"]["forma"])
									->setCellValue('F'.$i, $e["CentralCumplimiento"]["observacion"])
									->setCellValue('H'.$i, $_frecuencia_valor)
									->setCellValue('I'.$i, $e["Evaluacion"]["cumple_eval"])
		  						->setCellValue('J'.$i, $e["Evaluacion"]["evidencia_eval"])
		  						->setCellValue('L'.$i, $formula);

			$objPHPExcel->getActiveSheet()
									->getColumnDimension('B')
									->setWidth(50);

		  $objPHPExcel->getActiveSheet()
								  ->getRowDimension($i)
									->setRowHeight(-1);
		  $objPHPExcel->getActiveSheet()
									->getStyle("A$i:F$i")
									->applyFromArray($styleArray);
		  $objPHPExcel->getActiveSheet()
									->getStyle("H$i:J$i")
									->applyFromArray($styleArray);
		}
		unset($styleArray);
		$microtime = microtime();
		$objPHPExcel->getActiveSheet()->setTitle('LSoft - Reporte');
		@$objPHPExcel->getActiveSheet()
								->setCellValue('A6', 'Instalación: '. $central["nombre"])
								->setCellValue('A11', $tipoCuerpoLegal["nombre"]." ".$cuerpoLegal["numero"])
								->setCellValue('B11', $cuerpoLegal["descripcion"]);
		//$objPHPExcel->setActiveSheetIndex(0);
		@$fname = $central["nombre"]."_".$tipoCuerpoLegal["nombre"]."_".$cuerpoLegal["numero"]."_".$cuerpoLegal["descripcion"];
		$fname = str_replace(" ", "_", $fname);
		$fname .= "[".date("d.M.Y")."]";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$fname.'.xlsx"');
		header('Cache-Control: max-age=0');
	//	var_dump($objPHPExcel);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
	}
}
