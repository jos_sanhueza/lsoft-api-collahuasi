 <?php
App::uses('AppController', 'Controller');
/**
 * Leyes Controller
 *
 * @property Articulo $Articulo
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class LeyesController extends AppController {

/**
 * Components
 *
 * @var array
 */
  public $components = array('Paginator', 'RequestHandler', 'Session');

/**
* Método buscar
* Imprime objeto con resultados de búsqueda
* de LeyChile.cl, mediante Google API.
* @param cadena
* @return void
**/
  // public function buscar($cadena = null) {
  //   $search = "$cadena site:leychile.cl inurl:'Navegar?idNorma='";
  //   for ($i = 0; $i < 3; $i++)
  //   {
  //       $url =  "http://ajax.googleapis.com/ajax/services/"
  //               ."search/web?v=1.0&key=AIzaSyBacVRiPNo7uMqhtjXG4Zeq1DtSQA_UOD4"
  //               ."&cx=014517126046550339258:qoem7fagpyk&num=10"
  //               ."&start=".$i."&"."q=".str_replace(' ', '%20', $search);
  //       $ch = curl_init();
  //       curl_setopt($ch, CURLOPT_URL, $url);
  //       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //       curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
  //       $body = curl_exec($ch);
  //       curl_close($ch);
  //       $json[$i] = json_decode($body,true);
  //       foreach($json[$i]["responseData"]["results"] as $k=>$v)
  //       {
  //         $url = parse_url($v["unescapedUrl"]);
  //         parse_str($url["query"], $idNorma);
  //         if($idNorma["idNorma"])
  //         {
  //           $v["idNorma"] = $idNorma["idNorma"];
  //           $exists = $this->Ley->exists($idNorma["idNorma"]);
  //           $v["lsoft_inBd"] = $exists;
  //           $resultados[] = $v;
  //         }
  //       }
  //       //print_r($json);
  //   }
  //   $this->set("resultados", $resultados);
  //   $this->set("_serialize", "resultados");
  // }




/**
* Método view
* Imprime objeto JSON con el cotenido de un cuerpo legal,
* extraido desde LeyChile.cl
* @param idNorma
* @return void
**/
  public function view($idNorma){
    $xml = $this->Ley->obtxml($idNorma);
    $ley = $xml;
    $ley = $this->Ley->articulos($xml);
    $this->set(array("ley"=>$ley, "_serialize"=>"ley"));
  }

/**
 * add method
 *
 * @return void
 */
  public function add() {
    $idNorma = $this->request->data["idNorma"];
    $tematica_id = $this->request->data["tematica_id"];
    if(!$idNorma) throw new BadRequestException('Especifique idNorma.');
    if(!$tematica_id) throw new BadRequestException('Especifique tematica_id.');
    else {
      //$ley = $this->Ley->obtxml($idNorma);
      /**
      * Metadatos de la ley
      * @var metadatos
      **/
      $metadatos = $this->Ley->metadatos($idNorma);

      /**
       * Tipo de Norma y su respectivo número.
       * Ejemplo:
       * Decreto con Fuerza de Ley, 123
       */
      $tipo      = (string)$metadatos->Identificador->TiposNumeros->TipoNumero->Tipo;
      $numero    = (integer)$metadatos->Identificador->TiposNumeros->TipoNumero->Numero;

      /**
       * Busca si esque este tipo de norma existe.
       */
      $tipoCnt = $this->Ley->TipoCuerpoLegal->find("count",
        array(
          'conditions'=>array(
            'TipoCuerpoLegal.nombre'=>$tipo
          )
        )
      );

      /**
       * Si esque no existe el tipo, lo crea.
       */
      if($tipoCnt <= 0) {
        $nuevoTipo = array(
          "TipoCuerpoLegal"=>array(
            "nombre"=>$tipo
          )
        );

        if($_nuevo = $this->Ley->TipoCuerpoLegal->save($nuevoTipo)){
          $tipoId = $_nuevo["TipoCuerpoLegal"]["id"];
        }
      }
      /**
       * Si esque existe el tipo de norma, rescata su id.
       */
      else {
        $tipoId = $this->Ley->TipoCuerpoLegal->findByNombre($tipo);
        $tipoId = $tipoId["TipoCuerpoLegal"]["id"];
      }
      /**
       * Guardado del nuevo cuerpo legal
       * @var ley
       */
      $ley = array(
        //"CuerpoLegal"=>array(
          "idNorma"=>$idNorma,
          "tematica_id"=>$tematica_id,
          "tipo_cuerpo_legal_id"=>$tipoId,
          "numero"=>$numero,
          "nombre"=>(string)$metadatos->Metadatos->TituloNorma
        //)
      );
      if($ley = $this->Ley->save($ley)){
        // print_r($ley);
        $ley = $this->Ley->findByIdnorma($ley["Ley"]["idNorma"]);
        //  print_r($ley);die();
        $xml = $this->Ley->obtxml($idNorma);
        $articulos = $this->Ley->articulos($xml);
        $_articulos = array();
        foreach($articulos["noTransitorios"] as $k=>$v){
          // print_r($v->Atributos->idParte);die();
          $_a = array(
            "cuerpo_legal_id"=>$ley["Ley"]["id"],
            "numero"=>$v->Metadatos["NombreParte"],
            "texto"=>$v->Texto,
            "idParte"=>$v->Atributos->idParte,
            "ley_id"=>$idNorma,
            "transitorio"=>0
          );
          $_articulos[] = $_a;
        }
        foreach($articulos["transitorios"] as $k=>$v){
          // print_r($v->Atributos->idParte);die();
          $_a = array(
            "cuerpo_legal_id"=>$ley["Ley"]["id"],
            "numero"=>$v->Metadatos["NombreParte"],
            "texto"=>$v->Texto,
            "idParte"=>$v->Atributos->idParte,
            "ley_id"=>$idNorma,
            "transitorio"=>1
          );
          $_articulos[] = $_a;
        }

        /**
         * Guardar todos los articulos, si esque no existen.
         */

        $_sm = $this->Ley->Articulo->saveMany($_articulos);
        $ley = $this->Ley->findByIdnorma($ley["Ley"]["idNorma"]);
        //print_r($articulos);
        $this->set(array("ley"=>$ley, "_serialize"=>"ley"));
      }
    }
  }
  // fin add


/**
 * replace method
 *
 * @return void
 */

  public function replace($idNorma = null, $ley_id = null, $bool = false) {

    App::uses('AppShell', 'Console/Command');
    $shell = new AppShell();

    if(!$idNorma) throw new BadRequestException('Especifique idNorma.');
    else {
      $shell->out(__d('cake_console', "\n Normalizando cuerpo legal...\n"));

      //$ley = $this->Ley->obtxml($idNorma);
      /**
      * Metadatos de la ley
      * @var metadatos
      **/
      $metadatos = $this->Ley->metadatos($idNorma);

      /**
       * Tipo de Norma y su respectivo número.
       * Ejemplo:
       * Decreto con Fuerza de Ley, 123
       */
      $tipo      = (string)$metadatos->Identificador->TiposNumeros->TipoNumero->Tipo;
      $numero    = (integer)$metadatos->Identificador->TiposNumeros->TipoNumero->Numero;

      /**
       * Busca si esque este tipo de norma existe.
       */
      $tipoCnt = $this->Ley->TipoCuerpoLegal->find("count",
        array(
          'conditions'=>array(
            'TipoCuerpoLegal.nombre'=>$tipo
          )
        )
      );

      /**
       * Si esque no existe el tipo, lo crea.
       */
      if($tipoCnt <= 0) {
        $nuevoTipo = array(
          "TipoCuerpoLegal"=>array(
            "nombre"=>$tipo
          )
        );

        if($_nuevo = $this->Ley->TipoCuerpoLegal->save($nuevoTipo)){
          $tipoId = $_nuevo["TipoCuerpoLegal"]["id"];
        }
      }
      /**
       * Si esque existe el tipo de norma, rescata su id.
       */
      else {
        $tipoId = $this->Ley->TipoCuerpoLegal->findByNombre($tipo);
        $tipoId = $tipoId["TipoCuerpoLegal"]["id"];
      }
      /**
       * Guardado del nuevo cuerpo legal
       * @var ley
       */
      $ley = array(
        //"CuerpoLegal"=>array(
          "id"=>$ley_id,
          "idNorma"=>$idNorma,
          "tipo_cuerpo_legal_id"=>$tipoId,
          "numero"=>$numero,
          "nombre"=>(string)$metadatos->Metadatos->TituloNorma
        //)
      );








      $xml = $this->Ley->obtxml($idNorma);
      $articulos = $this->Ley->articulos($xml);
      //var_dump($xml, $articulos);

      //die("leyescontroller 271");


      }


      App::uses('CuerpoLegal', 'Model');
      $CuerpoLegal = new CuerpoLegal();



      if($ley = $CuerpoLegal->save($ley)){
        $_articulos = array();
        $_cntTrans = 0;
        $_cntNoTrans = 0;
        $_cntReemplazados = 0;


        if(
          isset($articulos["noTransitorios"])
          && !empty($articulos["noTransitorios"])
          && (count($articulos["noTransitorios"]) > 0)
        )
        {
          foreach($articulos["noTransitorios"] as $k=>$v){
            $_cntNoTrans ++;
            // print_r($v->Atributos->idParte);die();
            $this->Ley->Articulo->recursive = 0;
            $_articulo = $this->Ley->Articulo->findByNumeroAndCuerpoLegalId($v->Metadatos["NombreParte"], $ley_id);
            //var_dump($_articulo);

            $_a = array(
              "cuerpo_legal_id"=>$ley["CuerpoLegal"]["id"],
              "numero"=>$v->Metadatos["NombreParte"],
              "texto"=>$v->Texto,
              "idParte"=>$v->Atributos->idParte,
              "ley_id"=>$idNorma,
              "transitorio"=>0
            );
            if(count($_articulo) > 0){
              $_a["id"] = $_articulo["Articulo"]["id"];
              $shell->out(__d('cake_console', $_a["id"]." "));
              $_cntReemplazados++;
            }
            $_articulos[] = $_a;
          }
        }



        if(
          isset($articulos["transitorios"])
          && !empty($articulos["transitorios"])
          && (count($articulos["transitorios"]) > 0)
        )
        {
          foreach($articulos["transitorios"] as $k=>$v){
            $_cntTrans++;
            // print_r($v->Atributos->idParte);die();
            $_a = array(
              "cuerpo_legal_id"=>$ley["CuerpoLegal"]["id"],
              "numero"=>$v->Metadatos["NombreParte"],
              "texto"=>$v->Texto,
              "idParte"=>$v->Atributos->idParte,
              "ley_id"=>$idNorma,
              "transitorio"=>1
            );
            $_articulos[] = $_a;
          }
      }



      /**
       * Guardar todos los articulos, si esque no existen.
       */
      $_sm = $this->Ley->Articulo->saveMany($_articulos);
      $shell->out(__d('cake_console', "\nArticulos ingresados:".$_cntNoTrans+$_cntTrans));
      $shell->out(__d('cake_console', "Articulos no transitorios:".$_cntNoTrans));
      $shell->out(__d('cake_console', "Articulos transitorios:".$_cntTrans));
      $shell->out(__d('cake_console', "Articulos reemplazados:".$_cntReemplazados));
      $shell->in("Continuar [ enter ]");
      //print_r($articulos);

    }
  }
  // fin replace


  /**
  * Método normaliza
  */
  public function normaliza(){
    App::uses('AppShell', 'Console/Command');
    $shell = new AppShell();
    $cuerpoLegales = $this->Ley->find('all', array(
        'conditions' => array(
          "idNorma" => null
        )
      )
    );
    $x = 0;
    $options = array();
    $_options = array();
    $__options = array();
    foreach($cuerpoLegales as $k => $v)
    {
      $x++;
      //$shell->clear();
      $ley = $v["Ley"];
      //$cadena = $ley["descripcion"]." ".$ley["nombre"];
      $cadena = $ley["nombre"];
      //$shell->out("$x / ".count($cuerpoLegales)."(".round(($x*count($cuerpoLegales)/100),2)."%)");
      $shell->out($k.") ".$cadena);
      $shell->out($ley["descripcion"]."\n");
      $__options[] = $k;
    }
    $shell->out(__d('cake_console', "\n ".($x+1).") Salir\n"));
    $__choice = (int)$shell->in("Seleccione cuerpo legal a normalizar [ 1 ]: ", $__options, 1);
    if(true)
    {
      //$shell->clear();
      $ley = $cuerpoLegales[$__choice]["Ley"];
      //$cadena = $ley["descripcion"]." ".$ley["nombre"];
      $cadena = $ley["nombre"];

      $shell->out($cadena);
      $shell->out($ley["descripcion"]."\n");

      $_options = array();
      $shell->out(__d('cake_console', "\n 1: Buscar\n"));
      $shell->out(__d('cake_console', "\n 2: Proporcionar IdNorma\n"));

      $_options[] = 1;
      $_options[] = 2;
      $_choice = (int)$shell->in("Seleccione metodo de normalizacion: ", $_options, 1);
      if($_choice == 1)
      {
        $r = $this->buscar2($cadena, 5);

        $add = false;
        $choice = 0;

        if($r) {
          //var_dump($r);
          foreach($r as $_k=>$_v){
            $options[$_k] = $_k;
            $metadatos = $this->Ley->metadatos($_v["idNorma"]);

            $fecha = (string)$metadatos->Identificador["fechaPublicacion"];
            $tipo = (string)$metadatos->Identificador->TiposNumeros->TipoNumero->Tipo;
            $numero = (string)$metadatos->Identificador->TiposNumeros->TipoNumero->Numero;
            $titulo = (string)$metadatos->Metadatos->TituloNorma;
            $_opt = "$tipo $numero : $titulo ($fecha)";
            $shell->out(__d('cake_console', "\n " .$_k.': '.$_v["unescapedUrl"].":\n ".$_opt));
          }
          $shell->out(__d('cake_console', "\n ".($_k+1).": Ingreso manual de IdNorma"));
          $shell->out(__d('cake_console', "\n ".($_k+2).": Ninguno"));
          $options[$_k+1] = $_k+1;
          $options[$_k+2] = $_k+2;
          //$options = array(0,1,2);
          $shell->out("\n $cadena");
          $shell->out($ley["descripcion"]);
          $choice = (int)$shell->in("Seleccione resultado: ", $options, $_k+2);


          if($choice != ($_k+2))
          {
            if($choice != $_k+1){
              $add = $this->replace($r[$choice]["idNorma"], $ley["id"], true);
            }
            else
            {
              $_idNorma = "a";
              while(!is_numeric($_idNorma)){
                $_idNorma = $shell->in("Ingrese IdNorma:");
              }
              $add = $this->replace($_idNorma, $ley["id"], true);
            }
          }
        }
      //var_dump($add);
      //die();
      }
      else {
        $_idNorma = "a";
        while(!is_numeric($_idNorma)){
          $_idNorma = $shell->in("Ingrese IdNorma:");
        }
        $add = $this->replace($_idNorma, $ley["id"], true);
      }
    }

  }
  // fin normaliza

  /**
  * Método buscar2
  * Imprime objeto con resultados de búsqueda
  * de LeyChile.cl, mediante Google API.
  * @param cadena
  * @return void
  **/
  public function buscar2($cadena = null, $limit = 1) {
    $search = "$cadena site:leychile.cl inurl:'Navegar?idNorma='";
    $resultados = array();
    for ($i = 0; $i < $limit; $i++)
    {
      $url =  "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&key=AIzaSyBacVRiPNo7uMqhtjXG4Zeq1DtSQA_UOD4&cx=014517126046550339258:qoem7fagpyk&num=10&start=".$i."&"."q=".str_replace(' ', '%20', $search);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
      $body = curl_exec($ch);
      curl_close($ch);
      $json[$i] = json_decode($body,true);

      if
      (
        isset($json[$i]["responseData"]["results"])
        && !empty($json[$i]["responseData"]["results"])
        && (count($json[$i]["responseData"]["results"])>0)
      )
      {
        foreach($json[$i]["responseData"]["results"] as $k=>$v)
        {
          $url = parse_url($v["unescapedUrl"]);
          parse_str($url["query"], $idNorma);
          if(isset($idNorma) && isset($idNorma["idNorma"]) && $idNorma["idNorma"])
          {
            $v["idNorma"] = $idNorma["idNorma"];
            if(!$this->searchEnArray($v["idNorma"], $resultados)) $resultados[] = $v;
          }
        }
      }
      //print_r($json);
    }
    return $resultados;
  }

  /**
  * Método buscar
  * Imprime objeto con resultados de búsqueda
  * de LeyChile.cl, mediante Google API.
  * @param cadena
  * @return void
  **/
  public function buscar($cadena = null, $limit = 4) {
    $search = "$cadena site:leychile.cl inurl:'Navegar?idNorma='";
    $resultados = array();
    for ($i = 0; $i < $limit; $i++)
    {
      $url =  "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&key=AIzaSyBacVRiPNo7uMqhtjXG4Zeq1DtSQA_UOD4&cx=014517126046550339258:qoem7fagpyk&num=10&start=".$i."&"."q=".str_replace(' ', '%20', $search);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
      $body = curl_exec($ch);
      curl_close($ch);
      $json[$i] = json_decode($body,true);

      if
      (
        isset($json[$i]["responseData"]["results"])
        && !empty($json[$i]["responseData"]["results"])
        && (count($json[$i]["responseData"]["results"])>0)
      )
      {
        foreach($json[$i]["responseData"]["results"] as $k=>$v)
        {
          $url = parse_url($v["unescapedUrl"]);
          parse_str($url["query"], $idNorma);
          if(isset($idNorma) && isset($idNorma["idNorma"]) && $idNorma["idNorma"])
          {
            $v["idNorma"] = $idNorma["idNorma"];
            if(!$this->searchEnArray($v["idNorma"], $resultados))
            {
              $exists = $this->Ley->exists($idNorma["idNorma"]);
              $v["lsoft_inBd"] = $exists;
              $resultados[] = $v;
            }
          }
        }
      }
      //print_r($json);
    }
    $this->set("resultados", $resultados);
    $this->set("_serialize", "resultados");
  }


  /**
   * Este metodo debe salir de aca
   */
  function searchEnArray($id, $array) {
    if(
      isset($array)
      && !empty($array)
      && (count($array) > 0)
    )
    {
      foreach ($array as $key => $val) {
        //    $shell->out(__d('cake_console', "\n ".($_k+2).": Ninguno"));
        if ($val['idNorma'] === $id) {
          return true;
        }
      }
    }
    return false;
  }
}
