<?php
App::uses('AppController', 'Controller');
/**
 * CentralesCumplimientos Controller
 *
 * @property CentralesCumplimiento $CentralesCumplimiento
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CentralesCumplimientosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CentralesCumplimiento->recursive = 0;
		$this->set('centralesCumplimientos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CentralesCumplimiento->exists($id)) {
			throw new NotFoundException(__('Invalid centrales cumplimiento'));
		}
		$options = array('conditions' => array('CentralesCumplimiento.' . $this->CentralesCumplimiento->primaryKey => $id));
		$this->set('centralesCumplimiento', $this->CentralesCumplimiento->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CentralesCumplimiento->create();
			if ($this->CentralesCumplimiento->save($this->request->data)) {
				$this->Session->setFlash(__('The centrales cumplimiento has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The centrales cumplimiento could not be saved. Please, try again.'));
			}
		}
		$centrales = $this->CentralesCumplimiento->Central->find('list');
		$cumplimientos = $this->CentralesCumplimiento->Cumplimiento->find('list');
		$responsables = $this->CentralesCumplimiento->Responsable->find('list');
		$this->set(compact('centrales', 'cumplimientos', 'responsables'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->CentralesCumplimiento->exists($id)) {
			throw new NotFoundException(__('Ese registro no existe.'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($cc = $this->CentralesCumplimiento->save($this->request->data)) {
				$this->set("centrales_cumplimiento", $cc);
				$this->set("_serialize", "centrales_cumplimiento");
			} else {
				throw new BadRequestException("Error al guardar los datos.");
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CentralesCumplimiento->id = $id;
		if (!$this->CentralesCumplimiento->exists()) {
			throw new NotFoundException(__('Invalid centrales cumplimiento'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CentralesCumplimiento->delete()) {
			$this->Session->setFlash(__('The centrales cumplimiento has been deleted.'));
		} else {
			$this->Session->setFlash(__('The centrales cumplimiento could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
