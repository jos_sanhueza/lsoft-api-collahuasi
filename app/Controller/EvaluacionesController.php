<?php
App::uses('AppController', 'Controller');
/**
 * Evaluaciones Controller
 *
 * @property Evaluacion $Evaluacion
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class EvaluacionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Evaluacion->contain(
			array("CentralesCumplimiento"=>array(
						"Evidencia",
						"Cumplimiento"=>array(
							"Articulo"=>array(
								"CuerpoLegal"=>array(
									"CentralesCuerpoLegal"=>array(
										"Evidencia"
									)
								)
							),
							"Frecuencia"
						)
					),
				)
			);

		$this->set('evaluaciones', $this->Paginator->paginate());
		$this->set('_serialize','evaluaciones');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Evaluacion->exists($id)) {
			throw new NotFoundException(__('Invalid evaluacion'));
		}
		$options = array('conditions' => array('Evaluacion.' . $this->Evaluacion->primaryKey => $id));
		$this->set('evaluacion', $this->Evaluacion->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			$centrales_cumplimiento_id = $this->request->data["centrales_cumplimiento_id"];

			// $q_ultima_evaluacion = "select id, evidencia_eval
			// 												from evaluaciones Evaluacion
			// 												where
			// 													centrales_cumplimiento_id = $centrales_cumplimiento_id
			// 													order by id desc
			// 													limit 1";
			//
			// $ultima_evaluacion = $this->Evaluacion->query($q_ultima_evaluacion);
			$ultima_evaluacion = $this->Evaluacion->find("first",
			array(
					"contain"=>array(
						"CentralesCumplimiento"
					),
					"conditions"=>array(
						'CentralesCumplimiento.id'=>$centrales_cumplimiento_id
					),
					"order"=>"Evaluacion.id desc"
				)
			);

			//var_dump($ultima_evaluacion);

			if(isset($ultima_evaluacion["Evaluacion"]["evidencia_eval"]))
			$this->request->data["evidencia_eval"] = $ultima_evaluacion["Evaluacion"]["evidencia_eval"];


			$this->request->data["fecha"] = date("Y-m-d"); // Agrega la fecha actual

			$this->Evaluacion->create();
			if ($evaluacion = $this->Evaluacion->save($this->request->data)) {
				$this->set('evaluacion', $evaluacion);
				$this->set('_serialize', 'evaluacion');

			} else {
				throw new BadRequestException('Datos incorrectos.');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->Evaluacion->exists($id)) {
			throw new NotFoundException(__('Invalid evaluacion'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Evaluacion->save($this->request->data)) {
				$this->Session->setFlash(__('The evaluacion has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The evaluacion could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Evaluacion.' . $this->Evaluacion->primaryKey => $id));
			$this->request->data = $this->Evaluacion->find('first', $options);
		}
		$centralesCumplimientos = $this->Evaluacion->CentralesCumplimiento->find('list');
		$criticidades = $this->Evaluacion->Criticidad->find('list');
		$periodos = $this->Evaluacion->Periodo->find('list');
		$this->set(compact('centralesCumplimientos', 'criticidades', 'periodos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Evaluacion->id = $id;
		if (!$this->Evaluacion->exists()) {
			throw new NotFoundException(__('Esa Evaluacion no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->Evaluacion->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}

	public function evaluacionValidador(){
		$r = "Aquí debería ir info custom";
		$this->set("r", $r);
		$this->set("_serialize", "r");
	}
}
