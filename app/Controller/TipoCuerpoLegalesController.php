<?php
App::uses('AppController', 'Controller');
/**
 * TipoCuerpoLegales Controller
 *
 * @property TipoCuerpoLegal $TipoCuerpoLegal
 * @property PaginatorComponent $Paginator
 * @property "RequestHandler"Component $"RequestHandler"
 * @property SessionComponent $Session
 */
class TipoCuerpoLegalesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TipoCuerpoLegal->recursive = 0;
		$this->set('TipoCuerpoLegales', $this->Paginator->paginate());
		$this->set('_serialize', 'TipoCuerpoLegales');
	}

	/**
	* Entrega objeto con listado de tipos de cuerpo legal.
	* Sirve para llenar los SelectPicker.
	* @return null
	**/
	public function selectPicker() {
		$this->TipoCuerpoLegal->recursive = 0;
		$this->set('TipoCuerpoLegales', $this->TipoCuerpoLegal->find('all',
				array(
					'fields'=>array(
						'id',
						'nombre'
					),
					"conditions"=>array(
						"TipoCuerpoLegal.id >"=>204
					),
					"order"=>array(
						"TipoCuerpoLegal.nombre" => "asc"
					)
				)
			)
		);

		$this->set('_serialize', 'TipoCuerpoLegales');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TipoCuerpoLegal->exists($id)) {
			throw new NotFoundException(__('Invalid tipo riesgo'));
		}
		$options = array('conditions' => array('TipoCuerpoLegal.' . $this->TipoCuerpoLegal->primaryKey => $id));
		$this->set('TipoCuerpoLegal', $this->TipoCuerpoLegal->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TipoCuerpoLegal->create();
			if ($TipoCuerpoLegal = $this->TipoCuerpoLegal->save($this->request->data)) {
				$this->set("TipoCuerpoLegal", $TipoCuerpoLegal);
				$this->set("_serialize", "TipoCuerpoLegal");
			} else {
				throw new BadRequestException("No se ha podido guardar el tipo de cuerpo legal.");
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->TipoCuerpoLegal->exists($id)) {
			throw new NotFoundException(__('No existe ese tipo de cuerpo legal.'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($tipo_riesgo = $this->TipoCuerpoLegal->save($this->request->data)) {
				$this->set('tipo_riesgo', $tipo_riesgo);
				$this->set('_serialize', 'tipo_riesgo');

			} else {
				throw new BadRequestExcepcion("Error al guardar los datos del tipo de cuerpo legal.");
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->TipoCuerpoLegal->id = $id;
		if (!$this->TipoCuerpoLegal->exists()) {
			throw new NotFoundException(__('Ese Tipo de Cuerpo Legal no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->TipoCuerpoLegal->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}
}
