<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('DigestAuthenticate', 'Controller/Component/Auth');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'Session',
		'RequestHandler',
		'Acl',
		'Auth' => array(
			'authorize'=>array(
				'Actions'=>array('actionPath'=>'controllers')
			),
			'authenticate' => array(
				'Digest'=>array(
					'userModel'=>'Usuario',
					'realm'=>'lsoftColbun'
				)
			),
			'loginRedirect' => false,//array('controller'=>'usuarios', 'action'=>'check_login'),
			'unauthorizedRedirect' => false,
			'logoutRedirect' => false //array('controller'=>'usuarios', 'action'=>'index')
		)
	);

   /* public function isAuthorized($user) {
        return true;
    }*/

	public function beforeFilter()
	{
	   parent::beforeFilter();
		 //echo "beforefilter";
			//var_dump($_SESSION);
			CakeSession::write("Auth.Usuario", $this->Auth->user());
	   //AuthComponent::$sessionKey = false;
	   $this->RequestHandler->ext = 'json';
	   $this->Auth->allow("xls_ccl");
	   //$this->Auth->allow();
	   $this->Auth->allow("download");
		 $this->Auth->userModel = "Usuario";
			//$this->data["Session"] = $this->Auth;
     $this->response->header('Access-Control-Allow-Origin','*');
	   $this->response->header('Access-Control-Allow-Headers', 'Origin, Accept, Authorization, Content-Type,  Depth,  User-Agent,	X-File-Size,  X-Requested-With,	X-Requested-By, If-Modified-Since,	 X-File-Name,  Cache-Control');
       //$this->response->header("Access-Control-Allow-Headers: Authorization");
	   $this->response->header("Access-Control-Allow-Credentials: true");
	   $this->response->header("Access-Control-Expose-Headers: WWW-Authenticate");
	   $this->response->header('Access-Control-Allow-Headers','X-Requested-With');
     $this->response->header('Access-Control-Allow-Headers','Content-Type, x-xsrf-token');
	   $this->response->header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
     $this->response->header('Access-Control-Max-Age','172800');
	   if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
	            $this->response->header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
	        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			{
			    $this->response->header('Access-Control-Allow-Headers', 'Origin, Accept, Authorization, Content-Type,  Depth,  User-Agent,	X-File-Size,  X-Requested-With,	X-Requested-By, X-Request-Id,  If-Modified-Since,	 X-File-Name,  Cache-Control');
	         //   $this->response->header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
			}

	    }
	    //debug($this->response);
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			$this->response->send();
			exit();
		}
		//$this->response->send();
	}

	public function afterFilter(){
	//	echo "afterfilter";
	//	var_dump($this->Auth->user());
		CakeSession::write("Auth.Usuario", $this->Auth->user());
	}
}
