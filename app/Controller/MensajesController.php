<?php
App::uses('AppController', 'Controller');
/**
 * Mensajes Controller
 *
 * @property Mensaj $Mensaj
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class MensajesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Mensaj->contain(
			array
			(
				"Usuario",
				"Usuario2",
				"CentralesCumplimiento"=>array
				(
					"Central",
					"Cumplimiento"=>array
					(
						"Articulo"=>array
						(
							"CuerpoLegal"=>array
							(
								"Tematica"
							)
						)
					)
				)
			)
		);
		$this->paginate =
		array
		(
			"order"=>array("Mensaj.id"=>"desc")
		);
		
		$this->set('mensajes', $this->Paginator->paginate(
			)
		);
		$this->set('paging', $this->params["paging"]);
		$this->set("_serialize", array("mensajes", "paging"));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Mensaj->exists($id)) {
			throw new NotFoundException(__('Ese mensaje no existe.'));
		}
		$options = array('conditions' => array('Mensaj.' . $this->Mensaj->primaryKey => $id));
		$this->Mensaj->contain(
			array
			(
				"Usuario",
				"Usuario2",
				"CentralesCumplimiento"=>array
				(
					"Central",
					"Cumplimiento"=>array
					(
						"Articulo"=>array
						(
							"CuerpoLegal"=>array
							(
								"Tematica"
							)
						)
					)
				)
			)
		);
		$this->set('mensaje', $this->Mensaj->find('first', $options));
		$this->set("_serialize", "mensaje");
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Mensaj->create();
			if ($mensaje = $this->Mensaj->save($this->request->data)) {
				$this->set("mensaje", $mensaje);
				$this->set("_serialize", "mensaje");
			} else {
				throw new BadRequestException("Ocurrió un error al intentar enviar el mensaje.");
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->Mensaj->exists($id)) {
			throw new NotFoundException(__('No existe ese mensaje.'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($mensaje = $this->Mensaj->save($this->request->data)) {
			 	 $this->set("mensaje", $mensaje);
				 $this->set("_serialize", "mensaje");
			} else {
				throw new BadRequestException("Error al enviar la respuesta.");
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Mensaje->id = $id;
		if (!$this->Mensaje->exists()) {
			throw new NotFoundException(__('Ese Mensaje no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->Mensaje->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}
}
