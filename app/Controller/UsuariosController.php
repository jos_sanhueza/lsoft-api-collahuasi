<?php
App::uses('AppController', 'Controller');
App::uses('DigestAuthenticate', 'Controller/Component/Auth');
/**
 * Usuarios Controller
 *
 * @property Usuario $Usuario
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsuariosController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public $components = array('Paginator', 'Session', 'RequestHandler');

	public function beforeFilter()
	{
		parent::beforeFilter();
		CakeSession::write("Auth.Usuario", $this->Auth->user());
		//$this->Auth->allow("check_login");
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Usuario->recursive = 1;
		$usuarios = $this->Paginator->paginate();
		/*$_u = array();
		foreach($usuarios as $k=>$v){
			$_u[$k] = $v["Usuario"];
		}*/

		//ob_start("ob_gzhandler");
		foreach($usuarios as $k=>$v){
			unset($usuarios[$k]["Usuario"]["password"]);
		}
    $this->set("paging", $this->params["paging"]);
		$this->set('usuarios', $usuarios);
		$this->set("_serialize", array("usuarios", "paging"));
	}

	public function check_login(){

		// if(!($this->Auth->user())){
		// 	throw new BadRequestException('Las credenciales ingresadas son incorrectas.');
		// }

		$usuario = $this->Auth->user();
		$usuario["loged"] = true;
		$rol_id = $usuario["rol_id"];

		$q = "select id, alias from acos resultados
					where parent_id = 1
				";
		$res = $this->Usuario->query($q);

		$permisos = array();

		foreach($res as $k=>$v){
			//print_r($v);
			$aco_id = $v["resultados"]["id"];

			// $_c = (int)$v["resultados"]["_create"];
		  // $_r = (int)$v["resultados"]["_read"];
			// $_u = (int)$v["resultados"]["_update"];
			// $_d = (int)$v["resultados"]["_delete"];

			//$permisos[$v["resultados"]["alias"]]["_create"] = (bool)$_c;
			//$permisos[$v["resultados"]["alias"]]["_read"] = (bool)$_r;
			//$permisos[$v["resultados"]["alias"]]["_update"] = (bool)$_u;
			//$permisos[$v["resultados"]["alias"]]["_delete"] = (bool)$_d;

			$_q = "select * from (select aco.id, aco.alias, permisos._create, permisos._read, permisos._update, permisos._delete
			from aros_acos permisos
			join acos aco on permisos.aco_id = aco.id
			join aros ar on permisos.aro_id = ar.id
			where ar.foreign_key like $rol_id
			and aco.parent_id = $aco_id
			) as resultados
			";

			$_res = $this->Usuario->query($_q);

			foreach($_res as $_k => $_v) {
				$p = (int)$_v["resultados"]["_read"];
				$permisos[$v["resultados"]["alias"]][$_v["resultados"]["alias"]] = ($p>0) ? true : false;
			}

		}
		CakeSession::write("Auth.Usuario", $usuario);
		$usuario["Permisos"] = $permisos;
		$this->set("usuario", $usuario);
		$this->set("_serialize", "usuario");
	}

/*	public function login() {
	//No longer Auth Magic
	if ($this->Auth->login()) {
		return $this->redirect(array('controller' => 'usuarios', 'action' => 'index.json'));
	} else {
		$this->Session->setFlash(__('Username or password is incorrect'), 'default', array(), 'auth');
	}
		$this->autoRender = false;
	}*/

	public function logout() {
		$this->autoRender = false;
	    header('HTTP/1.1 401 Unauthorized');
		header("Location: http://logout@lsoft.cl/lsoft_api");
	}


 /**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Usuario->recursive = 1;
		if (!$this->Usuario->exists($id)) {
			throw new NotFoundException(__('Invalid usuario'));
		}
		$options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
		$usuario = $this->Usuario->find('first', $options);
		unset($usuario["Usuario"]["password"]);
		$this->set('usuario', $usuario);
		$this->set('_serialize', "usuario");
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if($this->request->is('post')) {
			$this->Usuario->create();
			if ($usuario = $this->Usuario->save($this->request->data)) {
				$usuario = $usuario["Usuario"];
				$this->set('usuario', $usuario);
				$this->set('_serialize', 'usuario');
			} else {
				throw new BadRequestException('Datos incorrectos.');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
public function edit() {
	$id = $this->request->data["id"];
	if (!$this->Usuario->exists($id)) {
		throw new NotFoundException(__('No existe ese Usuario.'));
	}
	if ($this->request->is(array('post', 'put'))) {
		if ($usuario = $this->Usuario->save($this->request->data)) {
			$usuario = $usuario["Usuario"];
			$this->set("usuario", $usuario);
			$this->set("_serialize", "usuario");
		} else {
			throw new BadRequestExcepcion("Error al guardar los datos del usuario.");
		}
	}
}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Usuario->id = $id;
		if (!$this->Usuario->exists()) {
			throw new NotFoundException(__('El usuario '.$id.' no existe.'));
		}
		$this->request->allowMethod('post', 'delete', 'get');
		try {
			$this->Usuario->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}

	public function avatar() {
		$this->autoRender = false;
		$res = array(
			"username"=>"colbun",
			"central"=> "Rucue",
			"porcentaje_cumplimiento"=> '70',
			"cnt_cuerpos_legales"=> '100',
			"cnt_tematicas"=> '9',
			"cnt_articulos"=> '9000'
		);
		$this->response->header('Access-Control-Allow-Origin', '*');
		echo json_encode($res);
	}
	public function mantenedorUsuarios(){
		$r = "Aquí debería ir info custom";
		$this->set("r", $r);
		$this->set("_serialize", "r");
	}
}
