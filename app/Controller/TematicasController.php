<?php
App::uses('AppController', 'Controller');
/**
 * Tematicas Controller
 *
 * @property Tematica $Tematica
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 */
class TematicasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Tematica->recursive = 0;
		$this->paginate = array(
												"order"=>array(
													"Tematica.nombre"=>"asc"
												)
											);
		$tematicas = $this->Paginator->paginate();
		$_t = array();
		foreach($tematicas as $k=>$v){
			$_t[$k] = $v["Tematica"];
		}

		//ob_start("ob_gzhandler");

		$this->set('tematicas', $_t);
		$this->set('paging', $this->params['paging']);
		$this->set("_serialize", array("tematicas", "paging"));
	}

/**
* Método selectPicker
*
* Entrega objeto con listado de artículos
* pertenecientes a un cuerpo legal.
* Sirve para llenar los SelectPicker.
**/

	public function selectPicker($central_id = null){
		$this->Tematica->recursive = 0;
		if($central_id == null){
			$this->set('tematicas', $this->Tematica->find('all',
						array(
								'fields'=>array('id', 'nombre'),
								'order'=>array('cast(nombre as unsigned)'=>'asc')
							)
						)
					);
		}
		else {
			$q = "select distinct Tematica.id, Tematica.nombre
						from tematicas Tematica
						join cuerpo_legales CuerpoLegal
						on CuerpoLegal.tematica_id = Tematica.id
						join articulos Articulo
						on Articulo.cuerpo_legal_id = CuerpoLegal.id
						join cumplimientos Cumplimiento
						on Cumplimiento.articulo_id = Articulo.id
						join centrales_cumplimientos CentralCumplimiento
						on CentralCumplimiento.cumplimiento_id = Cumplimiento.id
						where CentralCumplimiento.central_id = $central_id
						order by cast(Tematica.nombre as unsigned) asc
						";
			//debug($q);
			$tematicas = $this->Tematica->query($q);
			$this->set('tematicas', $tematicas);
		}
		$this->set('_serialize', 'tematicas');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Tematica->exists($id)) {
			throw new NotFoundException(__('Invalid tematica'));
		}
		$options = array('conditions' => array('Tematica.' . $this->Tematica->primaryKey => $id));
		$this->set('tematica', $this->Tematica->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Tematica->create();
			if ($tematica = $this->Tematica->save($this->request->data)) {
				$this->set("tematica", $tematica["Tematica"]);
				$this->set("_serialize", "tematica");
			}
		}
		// if($centralesCuerpoLegales = $this->Tematica->CentralesCuerpoLegal->find('list'))
		// {
		// 	$this->set(compact('centralesCuerpoLegales'));
		// }
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->Tematica->exists($id)) {
			throw new NotFoundException(__('Invalid tematica'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tematica->save($this->request->data)) {
				return $this->flash(__('The tematica has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('Tematica.' . $this->Tematica->primaryKey => $id));
			$this->request->data = $this->Tematica->find('first', $options);
		}
		$centralesCuerpoLegales = $this->Tematica->CentralesCuerpoLegal->find('list');
		$this->set(compact('centralesCuerpoLegales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Tematica->id = $id;
		if (!$this->Tematica->exists()) {
			throw new NotFoundException(__('Esa Tematica no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		// if ($this->Tematica->delete()) {
		// 	$this->response->statusCode(204);
		// } else {
		// 	$this->response->statusCode(500);
		// 	throw new InternalErrorException('Error al borrar.');
		// }

		try {
			$this->Tematica->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}

	public function mantenedorTematicas(){
		$r = "Aquí debería ir info custom";
		$this->set("r", $r);
		$this->set("_serialize", "r");
	}
}
