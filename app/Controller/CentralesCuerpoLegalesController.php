<?php
App::uses('AppController', 'Controller');
/**
 * CentralesCuerpoLegales Controller
 *
 * @property CentralesCuerpoLegal $CentralesCuerpoLegal
 * @property PaginatorComponent $Paginator
 * @property "RequestHandler"Component $"RequestHandler"
 * @property SessionComponent $Session
 */
class CentralesCuerpoLegalesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', '"RequestHandler"', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CentralesCuerpoLegal->recursive = 0;
		$this->set('centralesCuerpoLegales', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CentralesCuerpoLegal->exists($id)) {
			throw new NotFoundException(__('Invalid centrales cuerpo legal'));
		}
		$options = array('conditions' => array('CentralesCuerpoLegal.' . $this->CentralesCuerpoLegal->primaryKey => $id));
		$this->set('centralesCuerpoLegal', $this->CentralesCuerpoLegal->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CentralesCuerpoLegal->create();
			if ($this->CentralesCuerpoLegal->save($this->request->data)) {
				$this->Session->setFlash(__('The centrales cuerpo legal has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The centrales cuerpo legal could not be saved. Please, try again.'));
			}
		}
		$cuerpoLegales = $this->CentralesCuerpoLegal->CuerpoLegal->find('list');
		$centrales = $this->CentralesCuerpoLegal->Central->find('list');
		$evidencias = $this->CentralesCuerpoLegal->Evidencia->find('list');
		$tematicas = $this->CentralesCuerpoLegal->Tematica->find('list');
		$this->set(compact('cuerpoLegales', 'centrales', 'evidencias', 'tematicas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->CentralesCuerpoLegal->exists($id)) {
			throw new NotFoundException(__('Invalid centrales cuerpo legal'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CentralesCuerpoLegal->save($this->request->data)) {
				$this->Session->setFlash(__('The centrales cuerpo legal has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The centrales cuerpo legal could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CentralesCuerpoLegal.' . $this->CentralesCuerpoLegal->primaryKey => $id));
			$this->request->data = $this->CentralesCuerpoLegal->find('first', $options);
		}
		$cuerpoLegales = $this->CentralesCuerpoLegal->CuerpoLegal->find('list');
		$centrales = $this->CentralesCuerpoLegal->Central->find('list');
		$evidencias = $this->CentralesCuerpoLegal->Evidencia->find('list');
		$tematicas = $this->CentralesCuerpoLegal->Tematica->find('list');
		$this->set(compact('cuerpoLegales', 'centrales', 'evidencias', 'tematicas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CentralesCuerpoLegal->id = $id;
		if (!$this->CentralesCuerpoLegal->exists()) {
			throw new NotFoundException(__('Invalid centrales cuerpo legal'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CentralesCuerpoLegal->delete()) {
			$this->Session->setFlash(__('The centrales cuerpo legal has been deleted.'));
		} else {
			$this->Session->setFlash(__('The centrales cuerpo legal could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
