<?php
App::uses('AppController', 'Controller');
/**
 * Evidencias Controller
 *
 * @property Evidencia $Evidencia
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class EvidenciasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Evidencia->recursive = 0;
		$this->set('evidencias', $this->Paginator->paginate());
	}


public function beforeFilter(){
	parent::beforeFilter();
	$this->Auth->allow("add");
}
/**
 * Método centralesCuerpoLegales
 */

	public function centralesCuerpoLegales($central_id = null, $cuerpo_legal_id = null){
		$central_id = (isset($central_id)) ? $central_id : "'%'";
		$cuerpo_legal_id = (isset($cuerpo_legal_id)) ? $cuerpo_legal_id : "'%'";
		$q_evidencias = "select ccle.evidencia_id from centrales_cuerpo_legales_evidencias ccle
											join centrales_cuerpo_legales ccl
											on ccle.centrales_cuerpo_legal_id = ccl.id
											where ccl.central_id like $central_id and
											ccl.cuerpo_legal_id like $cuerpo_legal_id";
		$_evidencias = $this->Evidencia->query($q_evidencias);

		$cnt = sizeof($_evidencias);

		if($cnt > 0) {
			$__evidencias[] = "-1";
			foreach($_evidencias as $k=>$v) {
				$__evidencias[] = (string)$v["ccle"]["evidencia_id"];
			}
			//print_r($__evidencias);
			$this->Paginator->settings =
					array(
						"conditions"=>array(
							"Evidencia.id in" =>$__evidencias
						)
					);
			$this->Evidencia->contain();
			$evidencias = $this->Paginator->paginate();
		}
		else  $evidencias = array();
		$this->set("evidencias", $evidencias);
		$this->set('paging', $this->params["paging"]);
		$this->set("_serialize", array("evidencias", "paging"));
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Evidencia->exists($id)) {
			throw new NotFoundException(__('Invalid evidencia'));
		}
		$options = array('conditions' => array('Evidencia.' . $this->Evidencia->primaryKey => $id));
		$this->set('evidencia', $this->Evidencia->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
/**
  * Array
	*(
	*    [name] => _avatar.png
	*    [type] => image/png
	*	    [tmp_name] => /tmp/php2zgL5O
	*	    [error] => 0
	*	    [size] => 67008
	*	)
 */
	public function add() {
		$this->autoRender = false;
		if(!isset($this->data["centrales_cumplimiento_id"])) {
			throw new BadRequestException("Falta parámetro centrales_cumplimiento_id.");
		}
		else {
			if(isset($this->params["form"]["file"])){
				$file 		= $this->params["form"]["file"];
				$origen 	= $file["tmp_name"];
				$date 		= microtime(true);
				$_fileExtension = pathinfo($file["name"], PATHINFO_EXTENSION);
				$nuevoNombre = $date.".".$_fileExtension;
				$destino 	= APP."uploads".DS.$nuevoNombre;
				$_file 		= new File($origen);
				$_fileInfo = $_file->info();


				try
				{
					if(($_file->copy($destino, true)))
					{
						$this->Evidencia->create();
						$data = array(
							"Evidencia"=>array(
								"nombre"=>$file["name"],
								"ubicacion"=>$nuevoNombre,
								"mimetype"=>$_fileInfo["mime"]
							)
						);
						if($evidencia = $this->Evidencia->save($data)) {
							//	$this->set("evidencia", $evidencia);
							//	$this->set("_serialize", "evidencia");
							//	print_r($evidencia);
							$evidencia_id = $evidencia["Evidencia"]["id"];
							$centrales_cumplimiento_id = $this->data["centrales_cumplimiento_id"];
							$q_insert = "insert into centrales_cumplimientos_evidencias(evidencia_id, centrales_cumplimiento_id)
													values($evidencia_id, $centrales_cumplimiento_id)";
							$_res = $this->Evidencia->query($q_insert);
							/**$this->set("evidencia", $evidencia);
							$this->set("_serialize", "evidencia");**/
							echo json_encode($evidencia);
						}
					}
					else{
						throw new BadRequestException("Ocurrió un error al intentar subir la evidencia.");
					}
				}
				catch (Exception $e)
				{
					throw new BadRequestException("Ocurrió un error al intentar subir la evidencia.");
				}
			}
			else
			{
				throw new BadRequestException("No se ha recibido ningún archivo.");
			}
		}
	}
/**
 * Método Download
 */
	public function download($id = null) {
		if($this->Evidencia->exists($id)){
			$this->viewClass = 'Media';
			$evidencia = $this->Evidencia->findById($id);
			// debug($evidencia);
			$evidencia = $evidencia["Evidencia"];
			$filename = $evidencia["nombre"];
			$this->set(
			array(
				'id'				=> $evidencia["ubicacion"],
				'name'			=> pathinfo($evidencia["nombre"], PATHINFO_FILENAME),
				'extension'	=> pathinfo($evidencia["ubicacion"], PATHINFO_EXTENSION),
				'path' 			=> 'uploads'.DS,
				'download'	=>true
				)
			);
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->Evidencia->exists($id)) {
			throw new NotFoundException(__('Invalid evidencia'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Evidencia->save($this->request->data)) {
				$this->Session->setFlash(__('The evidencia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The evidencia could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Evidencia.' . $this->Evidencia->primaryKey => $id));
			$this->request->data = $this->Evidencia->find('first', $options);
		}
		$cumplimientos = $this->Evidencia->Cumplimiento->find('list');
		$centralesCuerpoLegales = $this->Evidencia->CentralesCuerpoLegal->find('list');
		$this->set(compact('cumplimientos', 'centralesCuerpoLegales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Evidencia->id = $id;
		if (!$this->Evidencia->exists()) {
			throw new NotFoundException(__('Esa Evidencia no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		// if ($this->Evidencia->delete()) {
		// 	$this->response->statusCode(204);
		// } else {
		// 	throw new BadRequestException('Error al borrar.');
		// }
		try {
			$this->Evidencia->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			throw new BadRequestException('Error al borrar.');
		}
	}

	public function evidenciaSgi(){
		$r = "Aquí debería ir info custom";
		$this->set("r", $r);
		$this->set("_serialize", "r");
	}
}
