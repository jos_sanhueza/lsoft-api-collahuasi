<?php
App::uses('AppController', 'Controller');
App::uses('Mensaj', 'Model');
/**
 * Cumplimientos Controller
 *
 * @property Cumplimiento $Cumplimiento
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class CumplimientosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
public function index($articulo_id = "%") {
	//$this->Cumplimiento->recursive = 0;
	$this->Paginator->settings = array(
		"conditions"=>array(
			"Cumplimiento.articulo_id like"=>$articulo_id
		)
	);

	$this->Cumplimiento->contain(array("Articulo", "Frecuencia", "Central"));
	$cumplimientos = $this->Paginator->paginate();

	//$cumplimientos = $this->Cumplimiento->query("select id from cumplimientos limit 261");
	$this->set('paging', $this->params['paging']);
	$this->set('cumplimientos', $cumplimientos);
	$this->set("_serialize", array("cumplimientos", "paging"));
}

/**
 * Método CentralesCuerpoLegales
 */
	public function centralesCuerpoLegales($central_id = null, $cuerpo_legal_id = null){
		$central_id = (isset($central_id)) ? $central_id : "'%'";
		$cuerpo_legal_id = (isset($cuerpo_legal_id)) ? $cuerpo_legal_id : "'%'";
		//$this->Cumplimiento->recursive = 0;

		// $q_articulos = "select distinct a.id from articulos a
		// 								join cuerpo_legales cl
		// 								on cl.id = a.cuerpo_legal_id
		// 								join centrales_cuerpo_legales ccl
		// 								on ccl.cuerpo_legal_id = cl.id
		// 								where cl.id like $cuerpo_legal_id
		// 								and ccl.central_id like $central_id";

		$q_cumplimientos = "select c.id from cumplimientos c
												join centrales_cumplimientos cc on c.id = cc.cumplimiento_id
												join articulos a on c.articulo_id = a.id
												join cuerpo_legales cl on cl.id = a.cuerpo_legal_id
												where cl.id = $cuerpo_legal_id
												and cc.central_id = $central_id
												and a.estado = 'activo'
												";

		//$articulos = $this->Cumplimiento->query($q_articulos);
		$__cumplimientos = $this->Cumplimiento->query($q_cumplimientos);
		if(sizeof($__cumplimientos) > 0) {
			$_cumplimientos[] = '-1';
			foreach($__cumplimientos as $k=>$v){
				$_cumplimientos[] = (string)$v['c']['id'];
			}
			if($_cumplimientos) {
				$this->Paginator->settings = array(
					"conditions"=>array(
						"Cumplimiento.id in" => $_cumplimientos
					)
				);
				$this->Cumplimiento->contain(
					array(
						"Articulo",
						"Frecuencia",
						"CentralesCumplimiento"=>array(
							"conditions"=>array(
								"central_id ="=>$central_id
							),
							"Criticidad",
							"Evidencia",
							"Mensaj",
							"Responsabl",
							"Evaluacion"=>array(
								"order"=>"Evaluacion.id desc"
								)
							),
							"Criticidad"));
				$cumplimientos = $this->Paginator->paginate();
				foreach($cumplimientos as $k=>$v){
					@$aux2 = $v["CentralesCumplimiento"][0];
					//print_r($aux2["observacion"]);die();
					$cumplimientos[$k]["CentralesCumplimiento"] = null;
					$cumplimientos[$k]["CentralesCumplimiento"]["id"] = $aux2["id"];
					$cumplimientos[$k]["CentralesCumplimiento"]["observacion"] = $aux2["observacion"];
					if(isset($aux2["Evaluacion"][0])){
						$aux = $aux2["Evaluacion"][0];
						$cumplimientos[$k]["CentralesCumplimiento"]["Evaluacion"] = $aux2["Evaluacion"][0];
					}
					if(isset($aux2["Criticidad"]) && count($aux2["Criticidad"]) > 0){
						$aux = $aux2["Criticidad"];
						$cumplimientos[$k]["CentralesCumplimiento"]["Criticidad"] = $aux2["Criticidad"];
					}
					else {
						//var_dump($v); die("muere");
						$cumplimientos[$k]["CentralesCumplimiento"]["Criticidad"] = $v["Criticidad"];
					}
					if(isset($aux2["Evidencia"]))
						$cumplimientos[$k]["CentralesCumplimiento"]["Evidencia"] = $aux2["Evidencia"];
						if(isset($aux2["Responsabl"]))
						$cumplimientos[$k]["CentralesCumplimiento"]["Responsable"] = $aux2["Responsabl"];
						if(isset($aux2["Mensaj"][0]))
						$cumplimientos[$k]["Mensajes"] = $aux2["Mensaj"];

				}
			}
		}
		else $cumplimientos = array();
		//$cumplimientos = $this->Cumplimiento->query("select id from cumplimientos limit 261");
		$this->set('cumplimientos', $cumplimientos);
		$this->set('paging', $this->params["paging"]);
		$this->set("_serialize", array("cumplimientos", "paging"));
	}


/**
 * Método custom de la pantalla
 */

	public function formaCumplimiento(){
		$r = "Aquí debería ir info custom";
		$this->set("r", $r);
		$this->set("_serialize", "r");
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cumplimiento->exists($id)) {
			throw new NotFoundException(__('Invalid cumplimiento'));
		}
		$options = array('conditions' => array('Cumplimiento.' . $this->Cumplimiento->primaryKey => $id));
		$this->set('cumplimiento', $this->Cumplimiento->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cumplimiento->create();
			if ($cumplimiento = $this->Cumplimiento->save($this->request->data)) {
				// debug($cumplimiento);
				$cumplimiento = $cumplimiento["Cumplimiento"];
				$observacion = $cumplimiento["observacion"];
				$id = $cumplimiento["id"];
				$q = "update centrales_cumplimientos set observacion = '$observacion' where cumplimiento_id = $id";
				$obs = $this->Cumplimiento->query($q);


				@$_cumplimiento = $this->Cumplimiento->find(
					"first",
					array(
						"conditions"=>[
							"Cumplimiento.id"=>$id
						],
						"contain"=>array(
							"CentralesCumplimiento"=>array(
								"Central"
							)
						)
					)
				);
				// debug($_cumplimiento);
				$msgData = array();
				$respuesta = "";
				foreach($_cumplimiento["CentralesCumplimiento"] as $k=>$v){
					 @$msgData[] = array(
						"usuario_id"=>$this->Auth->user('id'),
						"usuario_id2"=>$this->Auth->user('id'),
						"centrales_cumplimiento_id"=>$v["id"],
					 	"titulo"=>$v["Central"]["nombre"].": nueva obligación creada",
						"pregunta"=>"Nueva obligación",
						"respuesta"=>$respuesta
					);
				}
				$mensaje = new Mensaj();
				$mensaje->create();
				$msg = $mensaje->saveMany($msgData);
				$this->set('cumplimiento', $_cumplimiento);
				$this->set('_serialize', 'cumplimiento');

			} else {
				throw new BadRequestException('Datos incorrectos. > '.$cumplimiento);
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		// $this->set("cumplimiento", $this->request->data);
		// $this->set("_serialize", "cumplimiento");
		 $id = $this->request->data["id"];
		if (!$this->Cumplimiento->exists($id)) {
			throw new NotFoundException(__('Ese cumplimiento no existe.'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($cumplimiento = $this->Cumplimiento->save($this->request->data)) {
				$this->set("cumplimiento", $cumplimiento);
				$this->set("_serialize", "cumplimiento");
			} else {
				// $this->set("cumplimiento", $cumplimiento);
				// $this->set("_serialize", "cumplimiento");
			 	throw new BadRequestException("Error al guardar los datos de la obligacion.");
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Cumplimiento->id = $id;
		if (!$this->Cumplimiento->exists()) {
			throw new NotFoundException(__('Ese Cumplimiento no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->Cumplimiento->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}
}
