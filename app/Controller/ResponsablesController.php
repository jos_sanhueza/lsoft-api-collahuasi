<?php
App::uses('AppController', 'Controller');
/**
 * Responsables Controller
 *
 * @property Responsabl $Responsabl
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class ResponsablesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Responsabl->recursive = 0;
		$this->set('responsables', $this->Paginator->paginate());
	}

/**
* Método selectPicker
*
* Entrega objeto con listado de responsables
* existentes en base de datos.
*
* Sirve para llenar los SelectPicker.
**/

	public function selectPicker(){
		$this->Responsabl->recursive = 0;
		$this->set('responsables', $this->Responsabl->find('all',
					array(
							'fields'=>array('id', 'nombre'),
							'order'=>array('cast(nombre as unsigned)'=>'asc')
						)
					)
				);
		$this->set('_serialize', 'responsables');
	}



/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Responsabl->exists($id)) {
			throw new NotFoundException(__('Invalid responsabl'));
		}
		$options = array('conditions' => array('Responsabl.' . $this->Responsabl->primaryKey => $id));
		$this->set('responsabl', $this->Responsabl->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Responsabl->create();
			if ($this->Responsabl->save($this->request->data)) {
				$this->Session->setFlash(__('The responsabl has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The responsabl could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->Responsabl->exists($id)) {
			throw new NotFoundException(__('Invalid responsabl'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Responsabl->save($this->request->data)) {
				$this->Session->setFlash(__('The responsabl has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The responsabl could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Responsabl.' . $this->Responsabl->primaryKey => $id));
			$this->request->data = $this->Responsabl->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Responsabl->id = $id;
		if (!$this->Responsabl->exists()) {
			throw new NotFoundException(__('Ese Responsable no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->Responsabl->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}
}
