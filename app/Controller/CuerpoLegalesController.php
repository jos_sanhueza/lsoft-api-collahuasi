<?php
App::uses('AppController', 'Controller');
/**
 * CuerpoLegales Controller
 *
 * @property CuerpoLegal $CuerpoLegal
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class CuerpoLegalesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CuerpoLegal->recursive = 1;
		$this->set('cuerpoLegales', $this->Paginator->paginate());
		$this->set('paging', $this->params['paging']);
		$this->set('_serialize', array('cuerpoLegales', 'paging'));
	}

/**
 * Método buscar
 *
 * @return void
 */
/*	public function busar() {
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$cadena = isset($data["cadena"])? '%'.$data["cadena"].'%' : '%';
			$this->Paginator->settings = array('conditions'=>array(
																															"CuerpoLegal.nombre like " => "'$cadena'"
																														)
																													);

			$this->CuerpoLegal->recursive = 1;
			$this->set('cuerpoLegales', $this->Paginator->paginate();
			$this->set('paging', $this->params['paging']);
			$this->set('_serialize', array('cuerpoLegales', 'paging'));
		}
	}*/

/**
 * Método selectPicker
 *
 * Genera lista de Cuerpos Legales, para poblar selectPicker
 */
	public function selectPicker($tematica_id = null, $central_id = null) {
		$tematica_id = (isset($tematica_id)) ? $tematica_id : "'%'";
		$this->CuerpoLegal->recursive = 0;
		if($central_id != null ){
		$q = "select distinct CuerpoLegal.nombre, CuerpoLegal.id
					from cuerpo_legales CuerpoLegales
					join tematicas Tematica
					on CuerpoLegales.tematica_id = Tematica.id
					join (select distinct IFNULL(CONCAT(tcl.nombre , ' ',cl.numero, ': ', LOWER(cl.nombre)), cl.nombre) as nombre, cl.id as id,
					cl.estado as estado
					from cuerpo_legales cl
					join tipo_cuerpo_legales tcl
					on cl.tipo_cuerpo_legal_id = tcl.id
					) CuerpoLegal
					on CuerpoLegal.id = CuerpoLegales.id
					join articulos Articulo
					on Articulo.cuerpo_legal_id = CuerpoLegales.id
					join cumplimientos Cumplimiento
					on Cumplimiento.articulo_id = Articulo.id
					join centrales_cumplimientos CentralCumplimiento
					on CentralCumplimiento.cumplimiento_id = Cumplimiento.id
					where Tematica.id like $tematica_id
					and CuerpoLegal.estado = 'activo'
					and Articulo.estado = 'activo'
					and CentralCumplimiento.central_id like $central_id
					order by CuerpoLegal.nombre asc
					";
		}
		else {
			$q = "select distinct CuerpoLegal.nombre, CuerpoLegal.id
						from cuerpo_legales CuerpoLegales
						join tematicas Tematica
						on CuerpoLegales.tematica_id = Tematica.id
						join (
						select distinct IFNULL(CONCAT(tcl.nombre ,
						' ',cl.numero, ': ', LOWER(cl.nombre)), cl.nombre) as nombre, cl.id as id, cl.estado as estado
						from cuerpo_legales cl
						join tipo_cuerpo_legales tcl
						on cl.tipo_cuerpo_legal_id = tcl.id
						) CuerpoLegal
						on CuerpoLegal.id = CuerpoLegales.id
						left join articulos Articulo
						on Articulo.cuerpo_legal_id = CuerpoLegales.id
						where Tematica.id like $tematica_id
						and CuerpoLegal.estado = 'activo'
						and Articulo.estado = 'activo'
						order by CuerpoLegal.nombre asc
						";
		}
		//debug($q);
		// print_r($q);
		$cuerpoLegales = $this->CuerpoLegal->query($q);
		//$_filtro = array();
		/*foreach($filtro as $k=>$v) {
			$_filtro[$k] = $v["CentralCumplimiento"]["cuerpo_legal_id"];
		}
		$this->set('cuerpoLegales', $this->CuerpoLegal->find('all',
					array(
							'fields'=>array('id', 'nombre'),
							'order'=>array('nombre'=>'asc'),
							'conditions'=>array(
								"CuerpoLegal.id"=>$_filtro
							)
						)
					)
				);*/
		$this->set('cuerpoLegales', $cuerpoLegales);
		$this->set('_serialize', 'cuerpoLegales');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CuerpoLegal->exists($id)) {
			throw new NotFoundException(__('Invalid cuerpo legal'));
		}
		$options = array('conditions' => array('CuerpoLegal.' . $this->CuerpoLegal->primaryKey => $id));
		$this->set('cuerpoLegal', $this->CuerpoLegal->find('first', $options));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
		public function add() {
			if ($this->request->is('post')) {
				$this->CuerpoLegal->create();
				if ($cuerpo_legal = $this->CuerpoLegal->save($this->request->data)) {
					$this->set('cuerpo_legal', $cuerpo_legal);
					$this->set('_serialize', 'cuerpo_legal');

				} else {
					throw new BadRequestException('Datos incorrectos.');
				}
			}
		}

		/**
		 * edit method
		 *
		 * @throws NotFoundException
		 * @param string $id
		 * @return void
		 */
			public function edit() {
				$id = $this->request->data["id"];
				if (!$this->CuerpoLegal->exists($id)) {
					throw new NotFoundException(__('No existe ese cuerpo legal.'));
				}
				if ($this->request->is(array('post', 'put'))) {
					if ($cuerpo_legal = $this->CuerpoLegal->save($this->request->data)) {
						$this->set('cuerpo_legal', $cuerpo_legal);
						$this->set('_serialize', 'cuerpo_legal');
					} else {
						throw new BadRequestExcepcion("Error al guardar los datos del cuerpo legal.");
					}
				}
			}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->CuerpoLegal->id = $id;
		if (!$this->CuerpoLegal->exists()) {
			throw new NotFoundException(__('Ese Cuerpo Legal no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->CuerpoLegal->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}
}
