<?php
App::uses('AppController', 'Controller');
/**
 * Frecuencias Controller
 *
 * @property Frecuencia $Frecuencia
 * @property PaginatorComponent $Paginator
 * @property 'RequestHandler'Component $'RequestHandler'
 * @property SessionComponent $Session
 */
class FrecuenciasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Frecuencia->recursive = 0;
		$this->set('frecuencias', $this->Paginator->paginate());
		$this->set('_serialize', 'frecuencias');
	}

/**
* Entrega objeto con listado de tipos de frecuencias.
* Sirve para llenar los SelectPicker.
* @return null
*
**/
	public function selectPicker() {
		$this->Frecuencia->recursive = 0;
		$this->set('frecuencias', $this->Frecuencia->find('all',
				array(
					'fields'=>array(
						'id',
						'valor'
					)
				)
			)
		);
		$this->set('_serialize', 'frecuencias');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Frecuencia->exists($id)) {
			throw new NotFoundException(__('Invalid frecuencia'));
		}
		$options = array('conditions' => array('Frecuencia.' . $this->Frecuencia->primaryKey => $id));
		$this->set('frecuencia', $this->Frecuencia->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Frecuencia->create();
			if ($this->Frecuencia->save($this->request->data)) {
				$this->Session->setFlash(__('The frecuencia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The frecuencia could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->Frecuencia->exists($id)) {
			throw new NotFoundException(__('Invalid frecuencia'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Frecuencia->save($this->request->data)) {
				$this->Session->setFlash(__('The frecuencia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The frecuencia could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Frecuencia.' . $this->Frecuencia->primaryKey => $id));
			$this->request->data = $this->Frecuencia->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Frecuencia = false;
		$this->Frecuencia->id = $id;
		if (!$this->Frecuencia->exists()) {
			throw new NotFoundException(__('Esa Frecuencia no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->Frecuencia->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}
}
