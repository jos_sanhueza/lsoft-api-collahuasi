<?php
App::uses('AppController', 'Controller');
/**
 * Centrales Controller
 *
 * @property Central $Central
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CentralesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'RequestHandler');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Central->recursive = 0;
		$centrales = $this->Paginator->paginate();
	  /*$_c = array();
		foreach($centrales as $k=>$v){
			$_c[$k] = $v["Central"];
		}*/

		//ob_start("ob_gzhandler");*/
		//$_c = $centrales;
		$this->set('centrales', $centrales);
		$this->set("_serialize", "centrales");
	}

/**
* Método selectPicker
*
* Entrega objeto con listado de artículos
* pertenecientes a un cuerpo legal.
* Sirve para llenar los SelectPicker.
**/

	public function selectPicker(){
		$this->Central->recursive = 0;
		$usuario = $this->Auth->user();
		if($usuario["Rol"]["interno"]){
			if(is_null($usuario["central_id"])){
				$central = "%";
			}
			else {
				$central = $usuario["central_id"];
			}
		}
		else $central = "%";
		$this->set('centrales', $this->Central->find('all',
					array(
							'fields'=>array('id', 'nombre'),
							'order'=>array('cast(nombre as unsigned)'=>'asc'),
							'conditions'=>array(
								'id like'=>$central
							)

						)
					)
				);
		$this->set('_serialize', 'centrales');
	}
	/**
	* Retorna datos para la grilla de evaluaciones,
	* de la pantalla de evaluaciones.
	* va
	* @return array
	**/
	public function evaluaciones($central_id = '%', $cuerpo_legal_id = "%") {

		$this->Paginator->settings = array(
			'conditions'=>array(
				'Central.id like'=> "$central_id"
			)
		);
	/*	$articulos = $this->Central->query("select articulo_id from cumplimientos where id in( select cumplimiento_id from centrales_cumplimientos where central_id like '$central_id')");
		$_articulos = array();
		foreach($articulos as $k=>$v){
			$_articulos[$k] = $v["cumplimientos"]["articulo_id"];
		}
		debug($_articulos);
		die();*/
		$_articulos = array(1,2,3);
		$this->Central->contain(
			array(
				"CentralesCuerpoLegal"=>array(
				/*	//"Evidencia",
					"CuerpoLegal"=>array(
						"conditions"=>array(
							"CuerpoLegal.id like"=>$cuerpo_legal_id
						),
						"Articulo"=>array(
							"conditions"=>array(
								//"Articulo.id in"=>$_articulos
							),
							"Cumplimiento"=>array(
								//"Frecuencia",
								//"Evidencia"
							)
						)
					)*/
				)
			)
		);
		$inicio = microtime();
		$evaluaciones = $this->Paginator->paginate();//$this->Paginator->paginate();
		$fin = microtime();
		$total = $fin-$inicio;
		echo $total;
		$this->set("evaluaciones", $evaluaciones);
		/**
		* Línea necesaria para poder retornar status de la paginación.
		**/
		$this->set('paging', $this->params['paging']);

		$this->set("_serialize", array("evaluaciones", "paging"));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Central->exists($id)) {
			throw new NotFoundException(__('La central solicitada no existe.'));
		}
		$options = array('conditions' => array('Central.' . $this->Central->primaryKey => $id));
		$this->set('central', $this->Central->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Central->create();
			if ($central = $this->Central->save($this->request->data)) {
				$central = $central["Central"];
				$this->set('central', $central);
				$this->set('_serialize', 'central');
			} else {
				throw new BadRequestException('Datos incorrectos.');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->Central->exists($id)) {
			throw new NotFoundException(__('No existe esa Central.'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($central = $this->Central->save($this->request->data)) {
				$central = $central["Central"];
				$this->set("central", $central);
				$this->set("_serialize", "central");
			} else {
				throw new BadRequestExcepcion("Error al guardar los datos de la central.");
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Central->id = $id;
		if (!$this->Central->exists()) {
			throw new NotFoundException(__('Esa Central no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->Central->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}

	public function mantenedorCentrales(){
		$r = "Aquí debería ir info custom";
		$this->set("r", $r);
		$this->set("_serialize", "r");
	}
}
