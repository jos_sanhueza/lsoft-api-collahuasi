<?php
App::uses('AppController', 'Controller');
/**
 * Criticidades Controller
 *
 * @property Criticidad $Criticidad
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CriticidadesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');


	/**
	* Método selectPicker
	*
	* Entrega objeto con listado de artículos
	* pertenecientes a un cuerpo legal.
	* Sirve para llenar los SelectPicker.
	**/

	public function selectPicker(){
		$this->Criticidad->recursive = 0;
		$usuario = $this->Auth->user();
		$this->set('criticidades', $this->Criticidad->find('all',
			array(
				'fields'=>array('id', 'nombre'),
				'order'=>array('cast(nombre as unsigned)'=>'asc')
				)
			)
		);
		$this->set('_serialize', 'criticidades');
	}






/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Criticidad->recursive = 0;
		$this->set('criticidades', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Criticidad->exists($id)) {
			throw new NotFoundException(__('Invalid criticidad'));
		}
		$options = array('conditions' => array('Criticidad.' . $this->Criticidad->primaryKey => $id));
		$this->set('criticidad', $this->Criticidad->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Criticidad->create();
			if ($this->Criticidad->save($this->request->data)) {
				$this->Session->setFlash(__('The criticidad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The criticidad could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Criticidad->exists($id)) {
			throw new NotFoundException(__('Invalid criticidad'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Criticidad->save($this->request->data)) {
				$this->Session->setFlash(__('The criticidad has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The criticidad could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Criticidad.' . $this->Criticidad->primaryKey => $id));
			$this->request->data = $this->Criticidad->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Criticidad->id = $id;
		if (!$this->Criticidad->exists()) {
			throw new NotFoundException(__('Invalid criticidad'));
		}
		$this->request->allowMethod('post', 'delete');
		try {
			$this->Criticidad->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}
}
