<?php
App::uses('AppController', 'Controller');
/**
 * Articulos Controller
 *
 * @property Articulo $Articulo
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class ArticulosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Articulo->recursive = 0;
		$this->set('articulos', $this->Paginator->paginate());
		$this->set('paging', $this->params['paging']);
		$this->set('_serialize',array('articulos', 'paging'));
	}

	/**
	* Método selectPicker
	*
	* Entrega objeto con listado de artículos
	* pertenecientes a un cuerpo legal.
	* Sirve para llenar los SelectPicker.
	**/
	public function selectPicker($cuerpo_legal_id) {
		$this->Articulo->recursive = 0;
		$this->set('articulos', $this->Articulo->find('all',
				array(
					'fields'=>array(
						'id',
						'numero',
						'estado'
					),
					'conditions'=>array(
						'cuerpo_legal_id'=>$cuerpo_legal_id,
						'Articulo.estado'=>'activo'
					),
					'order'=>array(
						'numero'=>'asc'
					)
				)
			)
		);
		$this->set('_serialize', 'articulos');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Articulo->exists($id)) {
			throw new NotFoundException(__('El artículo solicitado no existe'));
		}
		$options = array('conditions' => array('Articulo.' . $this->Articulo->primaryKey => $id));
		$this->set('articulo', $this->Articulo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Articulo->create();
			if ($articulo = $this->Articulo->save($this->request->data)) {
				$this->set("articulo", $articulo);
				$this->set("_serialize", "articulo");
			} else {
				throw new BadRequestException('Datos incorrectos.');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit() {
		$id = $this->request->data["id"];
		if (!$this->Articulo->exists($id)) {
			throw new NotFoundException(__('Ese articulo no existe.'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($articulo = $this->Articulo->save($this->request->data)) {
				$this->set("articulo", $articulo);
				$this->set("_serialize", "articulo");
			} else {
				throw new BadRequestException("Error al guardar los datos del articulo.");
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Articulo->id = $id;
		if (!$this->Articulo->exists()) {
			throw new NotFoundException(__('Ese Articulo no existe.'));
		}
		$this->request->allowMethod('post', 'delete');
		// if ($this->Articulo->delete()) {
		// 	$this->response->statusCode(204);
		// } else {
		// 	throw new BadRequestException('Error al borrar.');
		// }
		try {
			$this->Articulo->delete();
			$this->response->statusCode(204);
		}
		catch (Exception $e){
			//$this->response->statusCode(204);
			throw new BadRequestException("Error al borrar.");
		}
	}
}
