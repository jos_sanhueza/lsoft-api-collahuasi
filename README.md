LSoft API :heart:
=======

API desarrollada en [CakePHP](http://www.cakephp.org).

Documentación
-------------------

LSoft API está diseñada para servir datos bajo el paradigma REST, [documentado en el libro de CakePHP] (http://book.cakephp.org/2.0/en/development/rest.html) . Cada servicio debería corresponder a una entidad lógica del modelo de datos de LSoft o a un conjunto de datos personalizados, requeridos para el despliegue de información crítica (tales como los reportes).

Consultas REST
-------------------
Para consultar cualquier servicio que corresponda a un controlador, debe hacerse como se muestra en la siguiente tabla, en la cual se utiliza como ejemplo el controlador [usuarios](usuarios):

>| # | Método HTTP  | URL de acceso         | Método invocado                          |
| -----|-------------------| ------------------------| -------------------------------------- |
| 1 | GET	                  | /usuarios                   | UsuariosController::index()            |
| 2 | GET	                  | /usuarios/123            | UsuariosController::view(123)       |
| 3 | POST	          | /usuarios                   | UsuariosController::add()              |
| 4 | PUT	                  | /usuarios/123            | UsuariosController::edit(123)        |
| 5 | DELETE	          | /usuarios/123            | UsuariosController::delete(123)    |
| 6 | POST	          | /usuarios/123            | UsuariosController::edit(123)        |

Contenidos
-------------

### 1. Servicios
   1. [Usuarios](usuarios)
   2. [Centrales](centrales)
   3. [Temáticas]
   4. [Cuerpos legales](cuerpo_legales)
   5. [Artículos](articulos)
   6. [Cumplimientos](cumplimientos)
   7. [Frecuencias](frecuencias)
   7. [Tipos de riesgo](tipo_riesgos)
   8. [Evaluaciones](evaluaciones)
   9. [Evidencias]
   10. [Reportes](reportes)

### 2. Digest Authentication

### 3. CORS

### 4. Enlaces


[http://www.lsoft.cl/](http://www.lsoft.cl/)
